#pragma once

#include "renderer_tensor.cuh"
#include "renderer_utils.cuh"

#include <forward_vector.h>
#include "renderer_cudad_bridge.cuh"
#include "renderer_adjoint.cuh"

/*
 * Defines by the host:
 * VOLUME_INTERPOLATION_GRID__REQUIRES_NORMAL
 * VOLUME_INTERPOLATION_GRID__USE_TENSOR
 * VOLUME_INTERPOLATION_GRID__TENSOR_TYPE float or double
 * VOLUME_INTERPOLATION_GRID__INTERPOLATION = 0->nearest, 1->trilinear, 2->tricubic
 * VOLUME_INTERPOLATION_GRID__OBJECT_SPACE
 */

namespace kernel
{
	struct VolumeInterpolationGridParameters
	{
#ifdef VOLUME_INTERPOLATION_GRID__USE_TENSOR
		using input_t = VOLUME_INTERPOLATION_GRID__TENSOR_TYPE;
		Tensor4Read<input_t> tensor;
#else
		cudaTextureObject_t tex;
#endif
		int3 resolutionMinusOne; //resolution-1
		real3 boxMin;
		real3 boxSize;
		real3 normalStep;
		real3 normalScale;
	};
}

__constant__ kernel::VolumeInterpolationGridParameters volumeInterpolationGridParameters;

namespace kernel
{
	struct VolumeGridOutput
	{
		real_t density;
		bool isInside;
	};

	struct VolumeInterpolationGrid
	{
		__host__ __device__ __forceinline__
		real3 getBoxMin() const
		{
			return volumeInterpolationGridParameters.boxMin;
		}
		__host__ __device__ __forceinline__
		real3 getBoxSize() const
		{
			return volumeInterpolationGridParameters.boxSize;
		}
		using density_t = real_t;
		
		__device__ __inline__ real_t sampleNearest(int3 posObject, int batch)
		{
#ifdef VOLUME_INTERPOLATION_GRID__USE_TENSOR
			//TODO, copy from DiffDVR
#else
			return tex3D<float>(volumeInterpolationGridParameters.tex, posObject.x, posObject.y, posObject.z);
#endif
		}
		__device__ __inline__ real_t sampleLinear(real3 posObject, int batch)
		{
#ifdef VOLUME_INTERPOLATION_GRID__USE_TENSOR
			//TODO, copy from DiffDVR
#else
			return tex3D<float>(volumeInterpolationGridParameters.tex, posObject.x, posObject.y, posObject.z);
#endif
		}

		//Source: https://github.com/DannyRuijters/CubicInterpolationCUDA
		// Inline calculation of the bspline convolution weights, without conditional statements
		template<class T> __device__ __inline__ void bspline_weights(T fraction, T& w0, T& w1, T& w2, T& w3)
		{
			const T one_frac = 1.0f - fraction;
			const T squared = fraction * fraction;
			const T one_sqd = one_frac * one_frac;

			w0 = 1.0f / 6.0f * one_sqd * one_frac;
			w1 = 2.0f / 3.0f - 0.5f * squared * (2.0f - fraction);
			w2 = 2.0f / 3.0f - 0.5f * one_sqd * (2.0f - one_frac);
			w3 = 1.0f / 6.0f * squared * fraction;
		}
		//Source: https://github.com/DannyRuijters/CubicInterpolationCUDA
		__device__ __inline__ real_t sampleCubic(real3 coord, int batch)
		{
			// shift the coordinate from [0,extent] to [-0.5, extent-0.5]
			const real3 coord_grid = coord - 0.5f;
			const real3 index = floor(coord_grid);
			const real3 fraction = coord_grid - index;
			float3 w0, w1, w2, w3;
			bspline_weights(fraction, w0, w1, w2, w3);

			const float3 g0 = w0 + w1;
			const float3 g1 = w2 + w3;
			const float3 h0 = (w1 / g0) - 0.5f + index;  //h0 = w1/g0 - 1, move from [-0.5, extent-0.5] to [0, extent]
			const float3 h1 = (w3 / g1) + 1.5f + index;  //h1 = w3/g1 + 1, move from [-0.5, extent-0.5] to [0, extent]

			// fetch the eight linear interpolations
			// weighting and fetching is interleaved for performance and stability reasons
			typedef float floatN; //return type
			floatN tex000 = sampleLinear(make_real3(h0.x, h0.y, h0.z), batch);
			floatN tex100 = sampleLinear(make_real3(h1.x, h0.y, h0.z), batch);
			tex000 = g0.x * tex000 + g1.x * tex100;  //weigh along the x-direction
			floatN tex010 = sampleLinear(make_real3(h0.x, h1.y, h0.z), batch);
			floatN tex110 = sampleLinear(make_real3(h1.x, h1.y, h0.z), batch);
			tex010 = g0.x * tex010 + g1.x * tex110;  //weigh along the x-direction
			tex000 = g0.y * tex000 + g1.y * tex010;  //weigh along the y-direction
			floatN tex001 = sampleLinear(make_real3(h0.x, h0.y, h1.z), batch);
			floatN tex101 = sampleLinear(make_real3(h1.x, h0.y, h1.z), batch);
			tex001 = g0.x * tex001 + g1.x * tex101;  //weigh along the x-direction
			floatN tex011 = sampleLinear(make_real3(h0.x, h1.y, h1.z), batch);
			floatN tex111 = sampleLinear(make_real3(h1.x, h1.y, h1.z), batch);
			tex011 = g0.x * tex011 + g1.x * tex111;  //weigh along the x-direction
			tex001 = g0.y * tex001 + g1.y * tex011;  //weigh along the y-direction

			return (g0.z * tex000 + g1.z * tex001);  //weigh along the z-direction
		}
		
		__device__ __inline__ real_t sample(real3 posObject, int batch)
		{
#if VOLUME_INTERPOLATION_GRID__INTERPOLATION==0
			return sampleNearest(make_int3(round(posObject)), batch);
#elif VOLUME_INTERPOLATION_GRID__INTERPOLATION==1
			return sampleLinear(posObject, batch);
#else
			return sampleCubic(posObject, batch);
#endif
		}
		
		__device__ __inline__ VolumeGridOutput eval(real3 position, int batch)
		{
			//the texture is adressed in unnormalized coordinates
#ifndef VOLUME_INTERPOLATION_GRID__OBJECT_SPACE
			//transform from [boxMin, boxMax] to [0, res]
			position = (position - volumeInterpolationGridParameters.boxMin) /
				volumeInterpolationGridParameters.boxSize *
				make_real3(volumeInterpolationGridParameters.resolutionMinusOne);
#endif
			bool isInside = all(position >= make_real3(0)) &&
				all(position <= make_real3(volumeInterpolationGridParameters.resolutionMinusOne));

			//now perform the interpolation
			real_t density = sample(position, batch);

			//done
			return VolumeGridOutput{ density, isInside };
		}

		__device__ __inline__ real3 evalNormal(real3 position, int batch)
		{
			//the texture is adressed in unnormalized coordinates
#ifndef VOLUME_INTERPOLATION_GRID__OBJECT_SPACE
			//transform from [boxMin, boxMax] to [0, res]
			position = (position - volumeInterpolationGridParameters.boxMin) /
				volumeInterpolationGridParameters.boxSize *
				make_real3(volumeInterpolationGridParameters.resolutionMinusOne);
#endif

			//compute normal
			real3 normal = make_real3(0);
#ifdef VOLUME_INTERPOLATION_GRID__REQUIRES_NORMAL
			normal.x = volumeInterpolationGridParameters.normalScale.x * (
				sample(position + make_real3(volumeInterpolationGridParameters.normalStep.x, 0, 0), batch) -
				sample(position - make_real3(volumeInterpolationGridParameters.normalStep.x, 0, 0), batch)
				);
			normal.y = volumeInterpolationGridParameters.normalScale.y * (
				sample(position + make_real3(0, volumeInterpolationGridParameters.normalStep.y, 0), batch) -
				sample(position - make_real3(0, volumeInterpolationGridParameters.normalStep.y, 0), batch)
				);
			normal.z = volumeInterpolationGridParameters.normalScale.z * (
				sample(position + make_real3(0, 0, volumeInterpolationGridParameters.normalStep.z), batch) -
				sample(position - make_real3(0, 0, volumeInterpolationGridParameters.normalStep.z), batch)
				);
#endif

			//done
			return normal;
		}
	};
}