#pragma once

#include "renderer_tensor.cuh"
#include "renderer_utils.cuh"

#include <forward_vector.h>
#include "renderer_cudad_bridge.cuh"
#include "renderer_adjoint.cuh"

/**
 * Defines:
 * RAY_EVALUATION_STEPPING__VOLUME_INTERPOLATION_T
 * RAY_EVALUATION_STEPPING__TRANSFER_FUNCTION_T
 * RAY_EVALUATION_STEPPING__BLENDING_T
 * RAY_EVALUATION_STEPPING__BRDF_T
 */

namespace kernel
{
	struct RayEvaluationSteppingDvrParameters
	{
		//TODO: find a way to make this dependent on the position
		real_t stepsize;
		real_t alphaEarlyOut;
		real_t densityMin;
		real_t densityMax;
	};
}

__constant__::kernel::RayEvaluationSteppingDvrParameters rayEvaluationSteppingDvrParameters;

namespace kernel
{
	struct RayEvaluationSteppingDvr
	{
		using VolumeInterpolation_t = RAY_EVALUATION_STEPPING__VOLUME_INTERPOLATION_T;
		using TF_t = RAY_EVALUATION_STEPPING__TRANSFER_FUNCTION_T;
		using Blending_t = RAY_EVALUATION_STEPPING__BLENDING_T;
		using BRDF_t = RAY_EVALUATION_STEPPING__BRDF_T;

		VolumeInterpolation_t volume;
		TF_t tf;
		Blending_t blending;
		BRDF_t brdf;

		__device__ __inline__
			RayEvaluationOutput eval(real3 rayStart, real3 rayDir, real_t tmax, int batch)
		{
			real_t tmin, tmax1;
			intersectionRayAABB(
				rayStart, rayDir, volume.getBoxMin(), volume.getBoxSize(),
				tmin, tmax1);
			tmin = rmax(tmin, 0);
			tmax = rmin(tmax1, tmax);

			//stepping
			const real_t stepsize = rayEvaluationSteppingDvrParameters.stepsize;
			const real_t alphaEarlyOut = rayEvaluationSteppingDvrParameters.alphaEarlyOut;
			const real_t densityMin = rayEvaluationSteppingDvrParameters.densityMin;
			const real_t densityMax = rayEvaluationSteppingDvrParameters.densityMax;
			const real_t divDensityRange = real_t(1) / (densityMax - densityMin);

			RayEvaluationOutput output{
				make_real4(0),
				make_real3(0),
				0.0
			};
			VolumeInterpolation_t::density_t previousDensity = { 0 };

			int terminationIndex;
			for (terminationIndex = 0; ; ++terminationIndex)
			{
				real_t tcurrent = tmin + terminationIndex * stepsize;
				if (tcurrent > tmax) break;
				if (output.color.w >= alphaEarlyOut) break;

				//sample volume
				real3 position = rayStart + rayDir * tcurrent;
				const auto [density, isInside] = volume.eval(position, batch);

				//transform density
				if (density >= densityMin)
				{
					auto density2 = (density - densityMin) * divDensityRange;
					auto n = volume.evalNormal(position, batch);

					//evaluate TF
					auto color1 = tf.eval(density2, n, previousDensity, stepsize, batch);
					previousDensity = density2;

					//BRDF
					auto color2 = brdf.eval(color1, position, n, rayDir, batch);

					//blending
					n = safeNormalize(n);
					RayEvaluationOutput newContribution = {
						color2,
						n,
						tcurrent
					};
					output = blending.eval(output, newContribution);
				}
			}

			return output;
		}
	};
}
