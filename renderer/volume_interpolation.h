#pragma once

#include "imodule.h"
#include "parameter.h"

#include "volume.h"
#include "background_worker.h"
#include <torch/types.h>
#include <filesystem>

BEGIN_RENDERER_NAMESPACE

/**
 * Defines the volume interpolation.
 * The volume interpolation is called in the following way:
 * <pre>
 * using density_t = Volume::density_t
 * const auto [density, isInside] = volume.eval(position, batch);
 * real3 normal 0 volume.evalNormal(position, batch)
 * real3 boxMin = volume.getBoxMin();
 * real3 boxSize = volume.getBoxSize();
 * </pre>
 * where position is either
 *  - in world space between \ref getBoxMin() and \ref getBoxMax()
 *  - in object space between [0,0,0] and \ref getObjectResolution(),
 *    if \ref supportsObjectSpaceIndexing() is true.
 * This is decided via \ref IKernelModule::GlobalSettings::interpolationInObjectSpace.
 *
 * Note that the returned normal is just the gradient of the density.
 * It is <b>not</b> normalized.
 */
class IVolumeInterpolation : public IKernelModule
{
protected:
	/**
	 * if object-space indexing is supported,
	 * i.e. objectResolution_ contains sensible data.
	 */
	const bool supportsObjectSpaceIndexing_;
	//The voxel resolution
	int3 objectResolution_;

	//Bounding box
	double3 boxMin_, boxMax_;

	IVolumeInterpolation(bool supportsObjectSpaceIndexing)
		: supportsObjectSpaceIndexing_(supportsObjectSpaceIndexing)
	{}

public:
	static constexpr std::string_view TAG = "volume";
	std::string getTag() const override { return std::string(TAG); }
	static std::string Tag() { return std::string(TAG); }

	[[nodiscard]] virtual bool supportsObjectSpaceIndexing() const
	{
		return supportsObjectSpaceIndexing_;
	}

	[[nodiscard]] virtual int3 objectResolution() const
	{
		return objectResolution_;
	}

	[[nodiscard]] virtual double3 boxMin() const
	{
		return boxMin_;
	}

	[[nodiscard]] virtual double3 boxMax() const
	{
		return boxMax_;
	}

	[[nodiscard]] virtual double3 boxSize() const;
	[[nodiscard]] virtual double3 voxelSize() const;

protected:
	virtual void setObjectResolution(const int3& objectResolution)
	{
		objectResolution_ = objectResolution;
	}

	virtual void setBoxMin(const double3& boxMin)
	{
		boxMin_ = boxMin;
	}
	
	virtual void setBoxMax(const double3& boxMax)
	{
		boxMax_ = boxMax;
	}
};
typedef std::shared_ptr<IVolumeInterpolation> IVolumeInterpolation_ptr;

//build-in volumes

/**
 * \brief volume interpolation using a regular grid of voxels.
 * The input can either be a 3D texture or a torch::Tensor.
 * Interpolation modes: nearest neighbor, trilinear, tricubic.
 */
class VolumeInterpolationGrid : public IVolumeInterpolation
{
public:
	enum class VolumeSource
	{
		VOLUME,
		TORCH_TENSOR,
		EMPTY
	};
	enum class VolumeInterpolation
	{
		NEAREST_NEIGHBOR,
		TRILINEAR,
		TRICUBIC,
		_COUNT_
	};
	static const std::string VolumeInterpolationNames[int(VolumeInterpolation::_COUNT_)];

	//Key for the UIStorage containing the histogram (Volume::Histogram_ptr)
	static const std::string UI_KEY_HISTOGRAM;
	//Key for the UIstorage containing the min density (float)
	static const std::string UI_KEY_MIN_DENSITY;
	//Key for the UIstorage containing the max density (float)
	static const std::string UI_KEY_MAX_DENSITY;

private:
	VolumeSource source_;
	VolumeInterpolation interpolation_;
	float minDensity_;
	float maxDensity_;

	//source = Texture
	Volume_ptr volume_;
	int mipmapLevel_;
	Volume::Histogram_ptr histogram_;

	//source = tensor
	Parameter<torch::Tensor> tensor_;

	//UI (source=Texture)
	std::filesystem::path volumeFullFilename_;
	static constexpr int MipmapLevels[] = { 0, 1, 2, 3, 7 };
	BackgroundWorker worker_;
	std::function<void()> backgroundGui_;
	bool newVolumeLoaded_ = false;
	//TODO: ensemble

public:
	VolumeInterpolationGrid();

	/**
	 * Sets the source to the volume and mipmap level
	 */
	void setSource(Volume_ptr v, int mipmap);
	/**
	 * Sets the source to the given tensor of shape
	 * (Batch, X, Y, Z)
	 */
	void setSource(const torch::Tensor& t);

	[[nodiscard]] VolumeSource source() const
	{
		return source_;
	}

	[[nodiscard]] VolumeInterpolation interpolation() const
	{
		return interpolation_;
	}

	void setInterpolation(const VolumeInterpolation interpolation)
	{
		interpolation_ = interpolation;
	}

	[[nodiscard]] float minDensity() const
	{
		return minDensity_;
	}

	[[nodiscard]] float maxDensity() const
	{
		return maxDensity_;
	}

	[[nodiscard]] Volume_ptr volume() const;

	[[nodiscard]] int mipmapLevel() const;

	[[nodiscard]] Volume::Histogram_ptr histogram() const
	{
		return histogram_;
	}

	[[nodiscard]] Parameter<torch::Tensor> tensor() const;

	std::string getName() const override;
	bool drawUI(UIStorage_t& storage) override;
	void load(const nlohmann::json& json) override;
	void save(nlohmann::json& json) const override;
private:
	void registerPybindModule(pybind11::module& m) override;
public:
	std::optional<int> getBatches(const GlobalSettings& s) const override;
	std::string getDefines(const GlobalSettings& s) const override;
	std::vector<std::string> getIncludeFileNames(const GlobalSettings& s) const override;
	std::string getConstantDeclarationName(const GlobalSettings& s) const override;
	std::string getPerThreadType(const GlobalSettings& s) const override;
	void fillConstantMemory(const GlobalSettings& s, CUdeviceptr ptr, CUstream stream) override;

private:
	//UI
	void loadVolumeDialog();
	void loadVolume(const std::string& filename, float* progress);
};

END_RENDERER_NAMESPACE
