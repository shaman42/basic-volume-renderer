#pragma once

#include "imodule.h"
#include "parameter.h"

BEGIN_RENDERER_NAMESPACE

/**
 * Base class of camera implementations:
 * <code>
 * const auto [rayStart, rayDir] camera.eval(real2 screenPos, int batch)
 * </code>
 * The screen position is defined in normalized device coordinates [-1,+1]^2.
 */
class ICamera : public IKernelModule
{
protected:
	//width / height
	double aspectRatio_ = 1.0;

public:
	static constexpr std::string_view TAG = "camera";
	static std::string Tag() { return std::string(TAG); }
	std::string getTag() const override { return Tag(); }


	[[nodiscard]] virtual double aspectRatio() const
	{
		return aspectRatio_;
	}

	virtual void setAspectRatio(const double aspectRatio)
	{
		this->aspectRatio_ = aspectRatio;
	}

	/**
	 * Returns the eye position / camera origin at the specific batch.
	 */
	virtual double3 getOrigin(int batch = 0) const = 0;
	/**
	 * Returns the front vector, i.e. the direction in which the camera faces,
	 * at the specific batch.
	 */
	virtual double3 getFront(int batch = 0) const = 0;
};
typedef std::shared_ptr<ICamera> ICamera_ptr;

/**
 * Camera on a sphere around a center facing inward.
 * The pitch, yaw, distance and center is differentiable and can be batched.
 */
class CameraOnASphere : public ICamera
{
public:
	enum Orientation
	{
		Xp, Xm, Yp, Ym, Zp, Zm
	};
	static const char* OrientationNames[6];
	static const float3 OrientationUp[6];
	static const int3 OrientationPermutation[6];
	static const bool OrientationInvertYaw[6];
	static const bool OrientationInvertPitch[6];

protected:
	Orientation orientation_;

	Parameter<double3> center_;
	//(pitch in degrees, yaw in degrees, distance)
	Parameter<double3> pitchYawDistance_;
	//field of view along Y in radians
	double fovYradians_;

	torch::Tensor cachedCameraMatrix_;
	
	//UI
	const float baseDistance_ = 1.0f;
	const float rotateSpeed_ = 0.5f;
	const float zoomSpeed_ = 1.1f;
	float zoomValue_ = 0;
	//cached for UI
	double3 cacheOrigin_;
	double3 cacheFront_;

public:
	CameraOnASphere();
	
	[[nodiscard]] virtual Orientation orientation() const
	{
		return orientation_;
	}

	virtual void setOrientation(const Orientation orientation)
	{
		orientation_ = orientation;
	}

	[[nodiscard]] virtual Parameter<double3> center() const
	{
		return center_;
	}
	[[nodiscard]] virtual Parameter<double3>& center()
	{
		return center_;
	}

	[[nodiscard]] virtual Parameter<double3> pitchYawDistance() const
	{
		return pitchYawDistance_;
	}
	[[nodiscard]] virtual Parameter<double3>& pitchYawDistance()
	{
		return pitchYawDistance_;
	}

	[[nodiscard]] virtual double fovYradians() const
	{
		return fovYradians_;
	}

	[[nodiscard]] virtual float zoomValue() const
	{
		return zoomValue_;
	}


	std::string getName() const override;
	bool drawUI(UIStorage_t& storage) override;
	void load(const nlohmann::json& json) override;
	void save(nlohmann::json& json) const override;
private:
	void registerPybindModule(pybind11::module& m) override;
public:
	std::optional<int> getBatches(const GlobalSettings& s) const override;
	std::vector<std::string> getIncludeFileNames(const GlobalSettings& s) const override;
	std::string getConstantDeclarationName(const GlobalSettings& s) const override;
	std::string getPerThreadType(const GlobalSettings& s) const override;
	void fillConstantMemory(const GlobalSettings& s, CUdeviceptr ptr, CUstream stream) override;

private:
	void computeParameters(
		double3& origin, double3& up, double& distance) const;

public:
	double3 getOrigin(int batch) const override;
	double3 getFront(int batch) const override;
};

END_RENDERER_NAMESPACE
