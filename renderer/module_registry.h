#pragma once

#include <unordered_map>
#include "imodule.h"

BEGIN_RENDERER_NAMESPACE

class ModuleRegistry
{
private:
	std::unordered_map<std::string, std::vector<IModule_ptr>> map_;

public:
	static ModuleRegistry& Instance();

	void registerModule(const std::string& tag, IModule_ptr module);
	static void RegisterModule(IModule_ptr module)
	{
		Instance().registerModule(module->getTag(), module);
	}

	void registerPybindModules(pybind11::module& m);

	/**
	 * \brief call upon startup to populate the modules.
	 * Auto-registration does not work because the renderer is
	 * usually compiled as static library.
	 */
	static void populateModules();

	const std::vector<IModule_ptr>& getModulesForTag(const std::string& tag);

	void loadAll(const nlohmann::json& root);
	void saveAll(nlohmann::json& root);
};

END_RENDERER_NAMESPACE
