#pragma once

#include <cuda_runtime.h>
#include <memory>
#include <string>
#include <functional>
#include <cassert>
#include <vector>
#include <optional>

#include "commons.h"

#ifdef _WIN32
#pragma warning( push )
#pragma warning( disable : 4251) // dll export of STL types
#endif

BEGIN_RENDERER_NAMESPACE

typedef std::function<void(const std::string&)> VolumeLoggingCallback_t;
typedef std::function<void(float)> VolumeProgressCallback_t;
typedef std::function<void(const std::string&, int)> VolumeErrorCallback_t;

class MY_API Volume
{
public:
	template<int numOfBins>
	struct VolumeHistogram
	{
		float bins[numOfBins]{ 0.0f };
		float minDensity{ FLT_MAX };
		float maxDensity{ 0.0f };
		float maxFractionVal{ 1.0f };
		unsigned int numOfNonzeroVoxels{ 0 };

		constexpr int getNumOfBins() const { return numOfBins; }
	};
	using Histogram = VolumeHistogram<512>;
	using Histogram_ptr = std::shared_ptr<Histogram>;

public:
	enum DataType
	{
		TypeUChar,
		TypeUShort,
		TypeFloat,
		_TypeCount_
	};
	static const int BytesPerType[_TypeCount_];

	class MY_API MipmapLevel
	{
	private:
		size_t sizeX_, sizeY_, sizeZ_;
		char* dataCpu_;
		cudaArray_t dataGpu_;
		cudaTextureObject_t dataTexLinear_;
		cudaTextureObject_t dataTexNearest_;
		int cpuDataCounter_;
		int gpuDataCounter_;

		Volume* parent_;
		friend class Volume;

		MipmapLevel(Volume* parent, size_t sizeX, size_t sizeY, size_t sizeZ);
	public:
		~MipmapLevel();

		MipmapLevel(const MipmapLevel& other) = delete;
		MipmapLevel(MipmapLevel&& other) noexcept = delete;
		MipmapLevel& operator=(const MipmapLevel& other) = delete;
		MipmapLevel& operator=(MipmapLevel&& other) noexcept = delete;

		size_t sizeX() const { return sizeX_; }
		size_t sizeY() const { return sizeY_; }
		size_t sizeZ() const { return sizeZ_; }
		int3 size() const { return make_int3(sizeX_, sizeY_, sizeZ_); }

		size_t idx(int x, int y, int z) const
		{
			assert(x >= 0 && x < sizeX_);
			assert(y >= 0 && y < sizeY_);
			assert(z >= 0 && z < sizeZ_);
			return x + sizeX_ * (y + sizeY_ * z);
		}

		template<typename T>
		const T* dataCpu() const { return reinterpret_cast<T*>(dataCpu_); }
		template<typename T>
		T* dataCpu() { cpuDataCounter_++; return reinterpret_cast<T*>(dataCpu_); }
		cudaArray_const_t dataGpu() const { return dataGpu_; }
		/**
		 * Returns the data as a 3d texture with un-normalized coordinates
		 * and linear interpolation.
		 */
		cudaTextureObject_t dataTexGpuLinear() const { return dataTexLinear_; }
		cudaTextureObject_t dataTexGpuNearest() const { return dataTexNearest_; }

		void copyCpuToGpu();
	};
	
private:
	
	double worldSizeX_, worldSizeY_, worldSizeZ_;
	DataType type_;
	std::vector<std::unique_ptr<MipmapLevel>> levels_;
	
public:
	Volume();
	Volume(DataType type, size_t sizeX, size_t sizeY, size_t sizeZ);
	~Volume();

	Volume(const Volume& other) = delete;
	Volume(Volume&& other) noexcept = delete;
	Volume& operator=(const Volume& other) = delete;
	Volume& operator=(Volume&& other) noexcept = delete;

	static constexpr int NO_COMPRESSION = 0;
	static constexpr int MAX_COMPRESSION = 9;
	
	/**
	 * Saves the volume to the file.
	 * Compression must be within [0, MAX_COMPRESSION],
	 * where 0 means no compression (equal to NO_COMPRESSION)
	 */
	void save(const std::string& filename,
		const VolumeProgressCallback_t& progress,
		const VolumeLoggingCallback_t& logging,
		const VolumeErrorCallback_t& error,
		int compression = NO_COMPRESSION) const;
	/**
	 * Loads and construct the volume
	 */
	Volume(const std::string& filename,
		const VolumeProgressCallback_t& progress,
		const VolumeLoggingCallback_t& logging,
		const VolumeErrorCallback_t& error);

	static std::shared_ptr<Volume> loadVolumeFromRaw(
		const std::string& file,
		const VolumeProgressCallback_t& progress,
		const VolumeLoggingCallback_t& logging,
		const VolumeErrorCallback_t& error,
		const std::optional<int>& ensemble = {});

	static std::shared_ptr<Volume> loadVolumeFromXYZ(
		const std::string& file,
		const VolumeProgressCallback_t& progress,
		const VolumeLoggingCallback_t& logging,
		const VolumeErrorCallback_t& error);

	/**
	 * Creates the histogram of the volume.
	 */
	[[nodiscard]] Volume::Histogram_ptr extractHistogram() const;

	double worldSizeX() const { return worldSizeX_; }
	double worldSizeY() const { return worldSizeY_; }
	double worldSizeZ() const { return worldSizeZ_; }
	void setWorldSizeX(double s) { worldSizeX_ = s; }
	void setWorldSizeY(double s) { worldSizeY_ = s; }
	void setWorldSizeZ(double s) { worldSizeZ_ = s; }
	DataType type() const { return type_; }

	int3 baseResolution() const {
		return make_int3(
			levels_[0]->sizeX(), levels_[0]->sizeY(), levels_[0]->sizeZ());
	}

	enum class MipmapFilterMode
	{
		/**
		 * Average filtering
		 */
		AVERAGE,
		/**
		 * A random sample is taken
		 */
		HALTON
	};
	
	/**
	 * \brief Creates the mipmap level specified by the given index.
	 * The level zero is always the original data.
	 * Level 1 is 2x downsampling, level 2 is 3x downsampling, level n is (n+1)x downsampling.
	 * This function does nothing if that level is already created.
	 * \param level the mipmap level
	 * \param filter the filter mode
	 */
	void createMipmapLevel(int level, MipmapFilterMode filter);

	typedef std::function<float(float3)> ImplicitFunction_t;

	/**
	 * \brief Creates a synthetic dataset using the
	 * implicit function 'f'.
	 * The function is called with positions in the range [boxMin, boxMax]
	 * (inclusive bounds), equal-spaced with a resolution of 'resolution'
	 * \param resolution the volume resolution
	 * \param boxMin the minimal coordinate
	 * \param boxMax the maximal coordinate
	 * \param f the generative function
	 * \return the volume
	 */
	static std::unique_ptr<Volume> createSyntheticDataset(
		int resolution, float boxMin, float boxMax,
		const ImplicitFunction_t& f);

	enum class ImplicitEquation
	{
		MARSCHNER_LOBB, //params "fM", "alpha"
		CUBE, //param "scale"
		SPHERE,
		INVERSE_SPHERE,
		DING_DONG,
		ENDRASS,
		BARTH,
		HEART,
		KLEINE,
		CASSINI,
		STEINER,
		CROSS_CAP,
		KUMMER,
		BLOBBY,
		TUBE,
		_NUM_IMPLICIT_EQUATION_
	};

	static std::unique_ptr<Volume> createImplicitDataset(
		int resolution, ImplicitEquation equation,
		const std::unordered_map<std::string, float>& params = {});

	/**
	 * Creates a volume from a raw buffer (e.g. from numpy).
	 *
	 * Indexing:
	 *     for(int x=0; x<sizes[0]; ++x) for (y...) for (z...)
	 *     float density = buffer[x*strides[0], y*strides[1], z*...];
	 *
	 * \param buffer the float buffer with the contents
	 * \param sizes the size of the volume in X, Y, Z
	 * \param strides the strides of the buffer in X, Y, Z
	 */
	static std::unique_ptr<Volume> createFromBuffer(
		const float* buffer, long long sizes[3], long long strides[3]);

private:
	bool mipmapCheckOrCreate(int level);
	void createMipmapLevelAverage(int level);
	void createMipmapLevelHalton(int level);

public:
	/**
	 * \brief Deletes all mipmap levels.
	 */
	void deleteAllMipmapLevels();

	/**
	 * \brief Returns the mipmap level specified by the given index.
	 * The level zero is always the original data.
	 * If the level is not created yet, <code>nullptr</code> is returned.
	 * Level 1 is 2x downsampling, level 2 is 3x downsampling, level n is (n+1)x downsampling
	 */
	const MipmapLevel* getLevel(int level) const;
	/**
	 * \brief Returns the mipmap level specified by the given index.
	 * The level zero is always the original data.
	 * If the level is not created yet, <code>nullptr</code> is returned.
	 * Level 1 is 2x downsampling, level 2 is 3x downsampling, level n is (n+1)x downsampling
	 */
	MipmapLevel* getLevel(int level);
};
typedef std::shared_ptr<Volume> Volume_ptr;

END_RENDERER_NAMESPACE

#ifdef _WIN32
#pragma warning( pop )
#endif
