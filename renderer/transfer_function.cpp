#include "transfer_function.h"

#include <filesystem>
#include "IconsFontAwesome5.h"
#include "portable-file-dialogs.h"
#include "renderer_utils.cuh"

const std::string renderer::ITransferFunction::UI_KEY_ABSORPTION_SCALING
	= "TF::absorptionScaling";
const std::string renderer::ITransferFunction::UI_KEY_COPIED_TF
= "TF::copiedTF";

bool renderer::ITransferFunction::drawUILoadSaveCopyPaste(UIStorage_t& storage)
{
	bool changed = false;
	
	std::string tfDirectory_ = ""; //TODO: load from settings
	if (ImGui::Button(ICON_FA_FOLDER_OPEN "##Load TF"))
	{
		// open file dialog
		auto results = pfd::open_file(
			"Load transfer function",
			tfDirectory_,
			{ "Transfer Function", "*.tf" },
			false
		).result();
		if (results.empty())
			return false;;
		std::string fileNameStr = results[0];

		auto fileNamePath = std::filesystem::path(fileNameStr);
		std::cout << "TF is loaded from " << fileNamePath << std::endl;
		tfDirectory_ = fileNamePath.string();

		//TODO
		throw std::runtime_error("not implemented yet");
		changed = true;
	}
	ImGui::SameLine();
	if (ImGui::Button(ICON_FA_SAVE "##Save TF"))
	{
		// save file dialog
		auto fileNameStr = pfd::save_file(
			"Save transfer function",
			tfDirectory_,
			{ "Transfer Function", "*.tf" },
			true
		).result();
		if (fileNameStr.empty())
			return false;

		auto fileNamePath = std::filesystem::path(fileNameStr);
		fileNamePath = fileNamePath.replace_extension(".tf");
		std::cout << "TF is saved under " << fileNamePath << std::endl;
		tfDirectory_ = fileNamePath.string();

		//TODO
		throw std::runtime_error("not implemented yet");
	}
	ImGui::SameLine();
	if (ImGui::Button(ICON_FA_COPY "##Copy TF"))
	{
		storage[UI_KEY_COPIED_TF] = shared_from_this();
		std::cout << "TF copied (as reference)" << std::endl;
	}
	ImGui::SameLine();
	ITransferFunction_ptr copiedTF = get_or(storage, UI_KEY_COPIED_TF,
		ITransferFunction_ptr());
	bool hasCopiedTF = static_cast<bool>(copiedTF);
	bool canPasteIntoCurrent = hasCopiedTF && canPaste(copiedTF);
	if (ImGui::ButtonEx(ICON_FA_PASTE " (?)##Paste TF", ImVec2(0,0), 
		!canPasteIntoCurrent ? ImGuiButtonFlags_Disabled : 0))
	{
		doPaste(copiedTF);
		storage.erase(UI_KEY_COPIED_TF);
		std::cout << "TF pasted" << std::endl;
		changed = true;
	}
	if (ImGui::IsItemHovered())
	{
		ImGui::BeginTooltip();
		ImGui::PushTextWrapPos(ImGui::GetFontSize() * 35.0f);
		if (canPasteIntoCurrent)
			ImGui::TextUnformatted("Pastes the copied TF into the current TF");
		else if (hasCopiedTF)
			ImGui::Text("TF copied, but cannot be pasted into the current TF\nThe TFs '%s' and '%s' are not compatible",
				copiedTF->getName(), this->getName());
		else
			ImGui::TextUnformatted("No TF was copied");
		ImGui::PopTextWrapPos();
		ImGui::EndTooltip();
	}
	return changed;
}

bool renderer::ITransferFunction::drawUIAbsorptionScaling(UIStorage_t& storage, double& scaling)
{
	if (const auto& it = storage.find(UI_KEY_ABSORPTION_SCALING);
		it != storage.end())
	{
		scaling = std::any_cast<double>(it->second);
	}

	bool changed = false;
	if (ImGui::SliderDouble("Opacity Scaling", &scaling, 1.0f, 500.0f, "%.3f", 2))
	{
		changed = true;
	}
	storage[UI_KEY_ABSORPTION_SCALING] = static_cast<double>(scaling);
	return changed;
}

void renderer::ITransferFunction::registerPybindModule(pybind11::module& m)
{
	//guard double registration
	static bool registered = false;
	if (registered) return;
	registered = true;
	
	namespace py = pybind11;
	py::class_<ITransferFunction, std::shared_ptr<ITransferFunction>>(m, "ITransferFunction");
}


const std::string renderer::detail::TFPartPiecewiseColor::UI_KEY_SHOW_COLOR_CONTROL_POINTS = "TFPartPiecewiseColor-ShowControlPoints";

renderer::detail::TFPartPiecewiseColor::TFPartPiecewiseColor()
{
	pointsUI_.push_back({ 0, 1,0,0 });
	pointsUI_.push_back({ 1,{1,1,1} });

	glBindTexture(GL_TEXTURE_2D, colorMapImage_);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, ColorMapWidth, 1, 0, GL_RGBA, GL_UNSIGNED_INT_8_8_8_8_REV, nullptr);
	glBindTexture(GL_TEXTURE_2D, 0);

	sortPointsAndUpdateTexture();
}

renderer::detail::TFPartPiecewiseColor::~TFPartPiecewiseColor()
{
	glDeleteTextures(1, &colorMapImage_);
}

bool renderer::detail::TFPartPiecewiseColor::drawUI(const ImRect& rect, bool showControlPoints)
{
	ImGuiWindow* window = ImGui::GetCurrentWindow();

	bool changed = false;
	if (handleIO(rect))
		changed = true;

	if (changed)
		sortPointsAndUpdateTexture();

	auto colorMapWidth = rect.Max.x - rect.Min.x;
	auto colorMapHeight = rect.Max.y - rect.Min.y;
	//draw background
	window->DrawList->AddImage((void*)colorMapImage_, rect.Min, rect.Max);

	if (showControlPoints)
	{
		//Draw the control points
		int cpIndex = 0;
		for (const auto& cp : pointsUI_)
		{
			//If this is the selected control point, use different color.
			auto rect2 = createControlPointRect(editorToScreen(cp.position, rect), rect);
			if (selectedControlPointForColor_ == cpIndex++)
			{
				window->DrawList->AddRect(rect2.Min, rect2.Max, ImColor(ImVec4(1.0f, 0.8f, 0.1f, 1.0f)), 16.0f, ImDrawCornerFlags_All, 3.0f);
			}
			else
			{
				window->DrawList->AddRect(rect2.Min, rect2.Max, ImColor(ImVec4(1.0f, 1.0f, 1.0f, 1.0f)), 16.0f, ImDrawCornerFlags_All, 2.0f);
			}
		}
	}

	return changed;
}

bool renderer::detail::TFPartPiecewiseColor::handleIO(const ImRect& rect)
{
	bool isChanged_ = false;

	auto mousePosition = ImGui::GetMousePos();

	if (selectedControlPointForColor_ >= 0)
	{
		auto& cp = pointsUI_[selectedControlPointForColor_];

		double3 pickedColor = make_double3(pickedColor_.x, pickedColor_.y, pickedColor_.z);
		if (any(cp.colorRGB != pickedColor))
		{
			cp.colorRGB = pickedColor;
			isChanged_ = true;
		}
	}

	//Early leave if mouse is not on color editor.
	if (!rect.Contains(mousePosition) && selectedControlPointForMove_ == -1)
	{
		return isChanged_;
	}

	//0=left, 1=right, 2=middle
	bool isLeftDoubleClicked = ImGui::IsMouseDoubleClicked(0);
	bool isLeftClicked = ImGui::IsMouseDown(0);
	bool isRightClicked = ImGui::IsMouseClicked(1);
	bool isLeftReleased = ImGui::IsMouseReleased(0);

	if (isLeftDoubleClicked)
	{
		isChanged_ = true;
		double3 pickedColor = make_double3(pickedColor_.x, pickedColor_.y, pickedColor_.z);
		pointsUI_.push_back({ screenToEditor(mousePosition.x, rect), pickedColor });
	}
	else if (isLeftClicked)
	{
		//Move selected point.
		if (selectedControlPointForMove_ >= 0)
		{
			isChanged_ = true;

			float center = std::min(std::max(mousePosition.x, rect.Min.x), rect.Max.x);

			pointsUI_[selectedControlPointForMove_].position = screenToEditor(center, rect);
		}
		//Check whether new point is selected.
		else
		{
			int size = pointsUI_.size();
			int idx;
			for (idx = 0; idx < size; ++idx)
			{
				auto cp = createControlPointRect(editorToScreen(pointsUI_[idx].position, rect), rect);
				if (cp.Contains(mousePosition))
				{
					selectedControlPointForColor_ = selectedControlPointForMove_ = idx;
					const auto& c = pointsUI_[idx].colorRGB;
					pickedColor_ = ImVec4(c.x, c.y, c.z, 1.0);
					break;
				}
			}

			//In case of no hit on any control point, unselect for color pick as well.
			if (idx == size)
			{
				selectedControlPointForColor_ = -1;
			}
		}
	}
	else if (isRightClicked)
	{
		int size = pointsUI_.size();
		int idx;
		for (idx = 0; idx < size; ++idx)
		{
			auto cp = createControlPointRect(editorToScreen(pointsUI_[idx].position, rect), rect);
			if (cp.Contains(mousePosition) && pointsUI_.size() > 1)
			{
				isChanged_ = true;

				pointsUI_.erase(pointsUI_.begin() + idx);
				selectedControlPointForColor_ = selectedControlPointForMove_ = -1;
				break;
			}
		}
	}
	else if (isLeftReleased)
	{
		selectedControlPointForMove_ = -1;
	}
	return isChanged_;
}

void renderer::detail::TFPartPiecewiseColor::updateControlPoints(const std::vector<Point>& points)
{
	selectedControlPointForMove_ = -1;
	selectedControlPointForColor_ = -1;
	pointsUI_.clear();
	pointsUI_.insert(pointsUI_.end(), points.begin(), points.end());
	sortPointsAndUpdateTexture();
}

std::vector<double3> renderer::detail::TFPartPiecewiseColor::getAsTexture(
	int resolution, ColorSpace colorSpace) const
{
	std::vector<double3> pixelData(resolution);
	int numPoints = static_cast<int>(pointsSorted_.size());

	for (int i = 0; i < resolution; ++i)
	{
		float density = (i + 0.5f) / resolution;
		//find control point
		int idx;
		for (idx = 0; idx < numPoints - 2; ++idx)
			if (pointsSorted_[idx + 1].position > density) break;
		//interpolate
		const float pLow = pointsSorted_[idx].position;
		const float pHigh = pointsSorted_[idx + 1].position;
		const auto vLow = pointsSorted_[idx].colorRGB;
		const auto vHigh = pointsSorted_[idx + 1].colorRGB;

		const float frac = clamp(
			(density - pLow) / (pHigh - pLow),
			0.0f, 1.0f);
		const auto v = (1 - frac) * vLow + frac * vHigh;
		pixelData[i] = v;
	}
	return pixelData;
}

ImRect renderer::detail::TFPartPiecewiseColor::createControlPointRect(float x, const ImRect& rect)
{
	return ImRect(ImVec2(x - 0.5f * cpWidth_, rect.Min.y),
		ImVec2(x + 0.5f * cpWidth_, rect.Max.y));
}

float renderer::detail::TFPartPiecewiseColor::screenToEditor(float screenPositionX, const ImRect& rect)
{
	float editorPositionX;
	editorPositionX = (screenPositionX - rect.Min.x) / (rect.Max.x - rect.Min.x);

	return editorPositionX;
}

float renderer::detail::TFPartPiecewiseColor::editorToScreen(float editorPositionX, const ImRect& rect)
{
	float screenPositionX;
	screenPositionX = editorPositionX * (rect.Max.x - rect.Min.x) + rect.Min.x;

	return screenPositionX;
}

void renderer::detail::TFPartPiecewiseColor::sortPointsAndUpdateTexture()
{
	pointsSorted_.clear();
	pointsSorted_.insert(pointsSorted_.end(), pointsUI_.begin(), pointsUI_.end());
	std::sort(pointsSorted_.begin(), pointsSorted_.end(),
		[](const Point& p1, const Point& p2)
		{
			return p1.position < p2.position;
		});


	std::vector<double3> tex = getAsTexture(ColorMapWidth, ColorSpace::RGB);
	std::vector<unsigned int> pixelData(ColorMapWidth);
	for (int i = 0; i < ColorMapWidth; ++i)
	{
		pixelData[i] = kernel::rgbaToInt(tex[i].x, tex[i].y, tex[i].z, 1.f);
	}

	glBindTexture(GL_TEXTURE_2D, colorMapImage_);
	glTexSubImage2D(
		GL_TEXTURE_2D, 0, 0, 0, ColorMapWidth, 1, GL_RGBA, GL_UNSIGNED_INT_8_8_8_8_REV,
		pixelData.data());
	glBindTexture(GL_TEXTURE_2D, 0);
}

renderer::detail::TfPartPiecewiseOpacity::TfPartPiecewiseOpacity()
	: pointsUI_({{0,0}, {1,1}})
{
	sortPoints();
}

bool renderer::detail::TfPartPiecewiseOpacity::drawUI(const ImRect& rect)
{
	bool changed = handleIO(rect);
	render(rect);
	return changed;
}

void renderer::detail::TfPartPiecewiseOpacity::updateControlPoints(const std::vector<Point>& points)
{
	pointsUI_.clear();
	pointsUI_.insert(pointsUI_.end(), points.begin(), points.end());
	sortPoints();
}

bool renderer::detail::TfPartPiecewiseOpacity::handleIO(const ImRect& rect)
{
	bool isChanged_ = false;

	auto mousePosition = ImGui::GetMousePos();

	//Early leave if mouse is not on opacity editor and no control point is selected.
	if (!rect.Contains(mousePosition) && selectedControlPoint_ == -1)
	{
		return isChanged_;
	}

	//0=left, 1=right, 2=middle
	bool isLeftDoubleClicked = ImGui::IsMouseDoubleClicked(0);
	bool isLeftClicked = ImGui::IsMouseDown(0);
	bool isRightClicked = ImGui::IsMouseClicked(1);
	bool isLeftReleased = ImGui::IsMouseReleased(0);

	if (isLeftDoubleClicked)
	{
		isChanged_ = true;

		pointsUI_.push_back(screenToEditor(mousePosition, rect));
	}
	else if (isLeftClicked)
	{
		//Move selected point.
		if (selectedControlPoint_ >= 0)
		{
			isChanged_ = true;

			ImVec2 center(std::min(std::max(mousePosition.x, rect.Min.x), rect.Max.x),
				std::min(std::max(mousePosition.y, rect.Min.y), rect.Max.y));

			pointsUI_[selectedControlPoint_] = screenToEditor(center, rect);
		}
		//Check whether new point is selected.
		else
		{
			int size = pointsUI_.size();
			for (int idx = 0; idx < size; ++idx)
			{
				auto cp = createControlPointRect(editorToScreen(pointsUI_[idx], rect));
				if (cp.Contains(mousePosition))
				{
					selectedControlPoint_ = idx;
					break;
				}
			}
		}
	}
	else if (isRightClicked)
	{
		int size = pointsUI_.size();
		for (int idx = 0; idx < size; ++idx)
		{
			auto cp = createControlPointRect(editorToScreen(pointsUI_[idx], rect));
			if (cp.Contains(mousePosition) && pointsUI_.size() > 1)
			{
				isChanged_ = true;

				pointsUI_.erase(pointsUI_.begin() + idx);
				selectedControlPoint_ = -1;
				break;
			}
		}
	}
	else if (isLeftReleased)
	{
		selectedControlPoint_ = -1;
	}

	if (isChanged_)
		sortPoints();
	return isChanged_;
}

void renderer::detail::TfPartPiecewiseOpacity::render(const ImRect& rect)
{
	//Draw the bounding rectangle.
	ImGuiWindow* window = ImGui::GetCurrentWindow();
	window->DrawList->AddRect(rect.Min, rect.Max, ImColor(ImVec4(0.3f, 0.3f, 0.3f, 1.0f)), 0.0f, ImDrawCornerFlags_All, 1.0f);

	std::vector<ImVec2> controlPointsRender;
	std::transform(pointsSorted_.begin(), pointsSorted_.end(),
		std::back_inserter(controlPointsRender),
		[this, rect](const Point& p)
		{
			return editorToScreen(p, rect);
		});

	//Draw lines between the control points.
	int size = controlPointsRender.size();
	for (int i = 0; i < size + 1; ++i)
	{
		auto left = (i == 0) ? ImVec2(rect.Min.x, controlPointsRender.front().y) : controlPointsRender[i - 1];
		auto right = (i == size) ? ImVec2(rect.Max.x, controlPointsRender.back().y) : controlPointsRender[i];

		window->DrawList->AddLine(left, right, ImColor(ImVec4(1.0f, 1.0f, 1.0f, 1.0f)), 1.0f);
	}

	//Draw the control points
	for (const auto& cp : controlPointsRender)
	{
		window->DrawList->AddCircleFilled(cp, circleRadius_, ImColor(ImVec4(0.0f, 1.0f, 0.0f, 1.0f)), 16);
	}
}

ImRect renderer::detail::TfPartPiecewiseOpacity::createControlPointRect(const ImVec2& controlPoint)
{
	return ImRect(ImVec2(controlPoint.x - circleRadius_, controlPoint.y - circleRadius_),
		ImVec2(controlPoint.x + circleRadius_, controlPoint.y + circleRadius_));
}

renderer::detail::TfPartPiecewiseOpacity::Point renderer::detail::TfPartPiecewiseOpacity::screenToEditor(const ImVec2& screenPosition, const ImRect& rect)
{
	float editorPositionX = (screenPosition.x - rect.Min.x) / (rect.Max.x - rect.Min.x);
	float editorPositionY = 1 - (screenPosition.y - rect.Min.y) / (rect.Max.y - rect.Min.y);
	return { editorPositionX, editorPositionY };
}

ImVec2 renderer::detail::TfPartPiecewiseOpacity::editorToScreen(const Point& editorPosition, const ImRect& rect)
{
	float screenPositionX = editorPosition.position * (rect.Max.x - rect.Min.x) + rect.Min.x;
	float screenPositionY = (1-editorPosition.absorption) * (rect.Max.y - rect.Min.y) + rect.Min.y;
	return { screenPositionX, screenPositionY };
}

void renderer::detail::TfPartPiecewiseOpacity::sortPoints()
{
	pointsSorted_.clear();
	pointsSorted_.insert(pointsSorted_.end(), pointsUI_.begin(), pointsUI_.end());
	std::sort(pointsSorted_.begin(), pointsSorted_.end(),
		[](const Point& p1, const Point& p2)
		{
			return p1.position < p2.position;
		});
}
