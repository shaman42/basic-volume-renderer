#include "image_evaluator_neural_texture.h"

#include <fstream>
#include <streambuf>
#include <tinyformat.h>



#include "IconsFontAwesome5.h"
#include "module_registry.h"
#include "portable-file-dialogs.h"
#include "pytorch_utils.h"

namespace
{
#pragma pack(push)
	struct Header
	{
		int8_t magic[4];
		int32_t version;
		int32_t flags;
		float radius;
		int32_t B;
		int32_t C;
		int32_t H;
		int32_t W;
		int32_t N;
		int8_t unused[28];
	};
#pragma pack(pop)

	struct membuf : std::streambuf
	{
		membuf(char* begin, char* end) {
			this->setg(begin, begin, end);
		}
	};
}

const std::string renderer::NeuralTextureModel::PREFERRED_EXTENSION = ".ntex";

renderer::NeuralTextureModel::NeuralTextureModel(
	int32_t flags, float radius, const torch::Tensor& texture,
	const torch::jit::Module& network)
	: flags_(flags)
	, radius_(radius)
	, texture_(texture)
	, network_(network)
{
	CHECK_DIM(texture, 4);
	CHECK_DTYPE(texture, c10::kFloat);
}

std::shared_ptr<renderer::NeuralTextureModel> renderer::NeuralTextureModel::load(const std::string& filename)
{
	static_assert(sizeof(Header) == HEADER_LENGTH, "Header structure is not packed properly");
	static_assert(sizeof(float) == 4, "Float does not occupy 4 bytes. What compiler did you smoke?");
	static_assert(sizeof(char) == 1, "char does not occupy 1 byte. What compiler did you smoke?");

	std::ifstream i(filename, std::ifstream::binary);

	//load header
	Header header = {};
	i.read(reinterpret_cast<char*>(&header), sizeof(header));
	if (memcmp(MAGIC, header.magic, 4)!=0)
	{
		std::string_view s1(reinterpret_cast<const char*>(MAGIC), 4);
		std::string_view s2(reinterpret_cast<const char*>(header.magic), 4);
		throw std::runtime_error(tinyformat::format(
			"Illegal magic number. Expected %s, got %s"
			, std::string(s1), std::string(s2)));
	}
	if (header.version != VERSION)
	{
		throw std::runtime_error(tinyformat::format(
			"Illegal version, supported is %d, but it is %d",
			VERSION, header.version));
	}

	//load texture data
	torch::Tensor texture = torch::empty({ header.B, header.C, header.H, header.W },
		at::TensorOptions().dtype(c10::kFloat));
	{
		const auto count = static_cast<int64_t>(header.B) * header.C * header.H * header.W;
		if (count * sizeof(float) != texture.nbytes())
			throw std::runtime_error("PyTorch added some weird padding, storage size does not match");
		i.read(reinterpret_cast<char*>(texture.data_ptr<float>()), sizeof(float) * count);
	}

	//load network
	torch::jit::Module network;
	{
		std::vector<char> data(header.N);
		i.read(data.data(), header.N);
		membuf sbuf(data.data(), data.data() + header.N);
		std::istream in(&sbuf);
		network = torch::jit::load(in);
	}

	//create instance
	return std::make_shared<NeuralTextureModel>(header.flags, header.radius, texture, network);
}

static void saveImpl(const std::string& filename, int32_t flags, float radius,
	const torch::Tensor& texture, const char* networkData, int32_t networkSize)
{
	//header
	Header header;
	memcpy(&header.magic[0], &renderer::NeuralTextureModel::MAGIC[0], 4);
	header.version = renderer::NeuralTextureModel::VERSION;
	header.flags = flags;
	header.radius = radius;
	header.B = texture.size(0);
	header.C = texture.size(1);
	header.H = texture.size(2);
	header.W = texture.size(3);
	header.N = networkSize;

	std::ofstream o(filename, std::ofstream::binary);
	o.write(reinterpret_cast<const char*>(&header), sizeof(header));

	const auto count = static_cast<int64_t>(header.B) * header.C * header.H * header.W;
	if (count * sizeof(float) != texture.nbytes())
		throw std::runtime_error("PyTorch added some weird padding, storage size does not match");
	o.write(reinterpret_cast<const char*>(texture.data_ptr<float>()), sizeof(float) * count);

	o.write(networkData, networkSize);
}

void renderer::NeuralTextureModel::save(const std::string& filename) const
{	
	//serialize network
	std::stringstream ss;
	std::streampos pos0 = ss.tellp();
	network_.save(ss);
	int64_t networkSize = ss.tellp() - pos0;
	if (networkSize > std::numeric_limits<int32_t>::max())
		throw std::runtime_error("Network is too large");
	const std::string networkData = ss.str();

	//save
	saveImpl(filename, flags_, radius_, texture_, networkData.data(), networkSize);
}

void renderer::NeuralTextureModel::toCuda()
{
	texture_ = texture_.to(c10::kCUDA);
	network_.to(c10::kCUDA);
}

void renderer::NeuralTextureModel::registerPybindModule(pybind11::module& m)
{
	namespace py = pybind11;
	py::class_<NeuralTextureModel, std::shared_ptr<NeuralTextureModel>> c(m, "NeuralTextureModel");
	c.def_property_readonly_static("Flag_BoundingBox", [](py::object /* self */) {return int(Flags::Flag_BoundingBox); })
	    .def_property_readonly_static("Flag_BoundingSphere", [](py::object /* self */) { return int(Flags::Flag_BoundingSphere); })
	    .def_property_readonly_static("Flag_BoundingOctahedron", [](py::object /* self */) { return int(Flags::Flag_BoundingOctahedron); })
	    .def_property_readonly_static("Flag_HasBackface", [](py::object /* self */) { return int(Flags::Flag_HasBackface); })
	    .def_property_readonly_static("Flag_HasLength", [](py::object /* self */) { return int(Flags::Flag_HasLength); });
	c.def_static("load", &NeuralTextureModel::load,
		py::arg("filename"), py::doc("Loads the neural texture model"))
		.def_static("save", [](const std::string& filename, int flags, float radius, const torch::Tensor& texture, const py::bytes& network)
			{
				const std::string s = static_cast<std::string>(network);
				saveImpl(filename, flags, radius, texture, s.data(), s.size());
			},
			py::arg("filename"), py::arg("flags"), py::arg("radius"), py::arg("texture"), py::arg("network"),
				py::doc(R"(
		Saves the network to the specified file.
		Because we can't pass a TorchScript directly to C++, it has to be serialized into a Bytes-object
		and then passed as bytes into this function.
		)"))
		.def("to_cuda", &NeuralTextureModel::toCuda,
			py::doc("Copies the weights+texture to CUDA. Required before rendering"))
		.def_readonly_static("extension", &NeuralTextureModel::PREFERRED_EXTENSION);
}


std::string renderer::ImageEvaluatorNeuralTexture::getName() const
{
	return "Neural Texture";
}

bool renderer::ImageEvaluatorNeuralTexture::drawUI(UIStorage_t& storage)
{
	bool changed = false;
	if (drawUIGlobalSettings(storage))
		changed = true;

	//camera
	ICamera_ptr camera = selectedCamera_;
	if (const auto& it = storage.find(UI_KEY_SELECTED_CAMERA);
		it != storage.end())
	{
		camera = std::any_cast<ICamera_ptr>(it->second);
	}
	const auto& cameras =
		ModuleRegistry::Instance().getModulesForTag(ICamera::Tag());
	if (camera == nullptr)
		camera = std::dynamic_pointer_cast<ICamera>(cameras[0]);
	if (ImGui::CollapsingHeader("Camera##IImageEvaluator", ImGuiTreeNodeFlags_DefaultOpen))
	{
		if (cameras.size() > 1) {
			for (int i = 0; i < cameras.size(); ++i) {
				const auto& name = cameras[i]->getName();
				if (ImGui::RadioButton(name.c_str(), cameras[i] == camera)) {
					camera = std::dynamic_pointer_cast<ICamera>(cameras[i]);
					changed = true;
				}
				if (i < cameras.size() - 1) ImGui::SameLine();
			}
		}
		if (camera->drawUI(storage))
			changed = true;
	}
	selectedCamera_ = camera;
	storage[UI_KEY_SELECTED_CAMERA] = camera;

	// model
	//UI
	if (ImGui::CollapsingHeader("Network##ImageEvaluatorNeuralTexture", ImGuiTreeNodeFlags_DefaultOpen))
	{
		int selectedModelIndex = selectedModelIndex_;
		std::vector<const char*> networkNames(loadedModels_.size());
		for (int i = 0; i < loadedModels_.size(); ++i)
			networkNames[i] = loadedModels_[i].shortFilename.c_str();
		selectedModelIndex = std::min(selectedModelIndex, static_cast<int>(networkNames.size()) - 1);
		if (ImGui::ListBox("", &selectedModelIndex, networkNames.data(), networkNames.size()))
		{
			selectedModelIndex_ = selectedModelIndex;
			changed = true;
		}
		ImGui::SameLine();
		ImGui::BeginGroup();
		if (ImGui::Button(ICON_FA_FOLDER_OPEN "##ImageEvaluatorNeuralTexture"))
		{
			loadModelUI();
			selectedModelIndex_ = static_cast<int>(networkNames.size()) - 1;
			changed = true;
		}
		if (ImGui::ButtonEx(ICON_FA_MINUS "##ImageEvaluatorNeuralTexture", ImVec2(0, 0),
			(loadedModels_.empty() || selectedModelIndex<0) ? ImGuiButtonFlags_Disabled : 0))
		{
			loadedModels_.erase(loadedModels_.begin() + selectedModelIndex_);
			selectedModelIndex_ = std::max(0, selectedModelIndex-1); //this assumes that the non-deletable are up front
			changed = true;
		}
		ImGui::EndGroup();
		
		//statistics
		if (selectedModel_)
		{
			const char* boundingObjectStr = 
				(selectedModel_->flags_&NeuralTextureModel::Flag_BoundingOctahedron)
				? "Octahedron"
				: ((selectedModel_->flags_ & NeuralTextureModel::Flag_BoundingSphere)
					? "Sphere" : "Box");
			const char* backfaceStr =
				(selectedModel_->flags_ & NeuralTextureModel::Flag_HasBackface)
				? "true" : "false";
			const char* lengthStr =
				(selectedModel_->flags_ & NeuralTextureModel::Flag_HasLength)
				? "true" : "false";
			ImGui::Text("Bonding Object: %s\nRadius: %.3f\nBackface: %s, Length: %s\nTexture: %d*%s*%s*%s",
				boundingObjectStr, selectedModel_->radius_, backfaceStr, lengthStr,
				static_cast<int>(selectedModel_->texture_.size(0)), static_cast<int>(selectedModel_->texture_.size(1)),
				static_cast<int>(selectedModel_->texture_.size(2)), static_cast<int>(selectedModel_->texture_.size(3)));
		}
	}

	if (selectedModelIndex_ >= 0 && selectedModelIndex_ < loadedModels_.size())
	{
		selectedModel_ = loadedModels_[selectedModelIndex_].model;
	}
	else
		selectedModel_ = nullptr;

	setSelectedChannel(ChannelMode::ChannelColor);
	drawUIOutputChannel(storage, true);
	return changed;
}

renderer::IModule_ptr renderer::ImageEvaluatorNeuralTexture::getSelectedModuleForTag(const std::string& tag)
{
	if (tag == ICamera::Tag())
		return selectedCamera_;
	return nullptr;
}

std::vector<std::string> renderer::ImageEvaluatorNeuralTexture::getSupportedTags()
{
	return { ICamera::Tag() };
}

torch::Tensor renderer::ImageEvaluatorNeuralTexture::render(int width, int height, CUstream stream,
	const torch::Tensor& out)
{
	if (!selectedModel())
		throw std::runtime_error("No model loaded, can't render");

	//Still part of active research
	throw std::runtime_error("Not implemented yet");
}

void renderer::ImageEvaluatorNeuralTexture::load(const nlohmann::json& json)
{
	std::string cameraName = json.value("selectedCamera", "");
	const auto& cameras =
		ModuleRegistry::Instance().getModulesForTag(ICamera::Tag());
	for (const auto v : cameras)
	{
		if (v->getName() == cameraName) {
			selectedCamera_ = std::dynamic_pointer_cast<ICamera>(v);
			break;
		}
	}

	selectedModelIndex_ = json["selectedModelIndex"];
	std::vector<std::string> names = json["loadedModels"];
	loadedModels_.clear();
	for (const auto& n : names)
	{
		auto om = loadModel(n);
		if (om.has_value())
			loadedModels_.push_back(om.value());
	}
}

void renderer::ImageEvaluatorNeuralTexture::save(nlohmann::json& json) const
{
	json["selectedCamera"] = selectedCamera_ ? selectedCamera_->getName() : "";
	json["selectedModelIndex"] = selectedModelIndex_;
	std::vector<std::string> names;
	for (const auto& m : loadedModels_)
		names.push_back(m.fullFilename);
}

void renderer::ImageEvaluatorNeuralTexture::registerPybindModule(pybind11::module& m)
{
	IImageEvaluator::registerPybindModule(m);

	//guard double registration
	static bool registered = false;
	if (registered) return;
	registered = true;
	
	namespace py = pybind11;
	py::class_<ImageEvaluatorNeuralTexture, IImageEvaluator, std::shared_ptr<ImageEvaluatorNeuralTexture>>(m, "ImageEvaluatorNeuralTexture")
		.def_readwrite("camera", &ImageEvaluatorNeuralTexture::selectedCamera_,
			py::doc("The selected ICamera method"))
		.def_property("model", 
			&ImageEvaluatorNeuralTexture::selectedModel,
			&ImageEvaluatorNeuralTexture::setSelectedModel,
			py::doc("The selected NeuralTextureModel"));

	NeuralTextureModel::registerPybindModule(m);
}

std::optional<renderer::ImageEvaluatorNeuralTexture::LoadedModel>
renderer::ImageEvaluatorNeuralTexture::loadModel(
	const std::string& fullFilename)
{
	try
	{
		auto m = NeuralTextureModel::load(fullFilename);
		m->toCuda();
		return LoadedModel{
			m,
			std::filesystem::absolute(std::filesystem::path(fullFilename)).string(),
			std::filesystem::path(fullFilename).stem().string()
		};
	}
	catch (const std::exception& ex)
	{
		std::cerr << "Unable to load model " << fullFilename << ":\n" << ex.what() << std::endl;
		return {};
	}
}

void renderer::ImageEvaluatorNeuralTexture::loadModelUI()
{
	// open file dialog
	static std::string default_path;
	auto results = pfd::open_file(
		"Load super-resolution network",
		default_path,
		{ "", "*" + NeuralTextureModel::PREFERRED_EXTENSION },
		true
	).result();

	bool settingsWritten = false;
	for (const auto& fileNameStr : results)
	{
		std::cout << "Load " << fileNameStr << std::endl;
		if (!settingsWritten)
		{
			default_path = absolute(std::filesystem::path(fileNameStr).parent_path()).string();
			//ImGui::MarkIniSettingsDirty();
			//ImGui::SaveIniSettingsToDisk(GImGui->IO.IniFilename);
			settingsWritten = true;
		}
		auto om = loadModel(fileNameStr);
		if (om.has_value())
			loadedModels_.push_back(om.value());
	}
}
