#include <cuda.h>
#include <GL/glew.h>
#include <cuMat/src/Context.h>

#include "renderer_tensor.cuh"
#include "helper_math.cuh"
#include "renderer_utils.cuh"

namespace kernel
{
	template<typename T>
	__device__ inline float fetchChannel(
		const Tensor4Read<T>& input,
		int channel, int x, int y)
	{
		if (channel == -1) return 0;
		if (channel == -2) return 1;
		return input[0][channel][y][x];
	}
	template<typename T>
	__global__ void SelectOutputChannelKernel(
		dim3 virtual_size,
		Tensor4Read<T> input,
		unsigned int* output,
		int rId, int gId, int bId, int aId,
		float scaleRGB, float offsetRGB, float scaleA, float offsetA)
	{
		CUMAT_KERNEL_2D_LOOP(x, y, virtual_size)
		{
			float r = fetchChannel(input, rId, x, y) * scaleRGB + offsetRGB;
			float g = fetchChannel(input, gId, x, y) * scaleRGB + offsetRGB;
			float b = fetchChannel(input, bId, x, y) * scaleRGB + offsetRGB;
			float a = fetchChannel(input, aId, x, y) * scaleA + offsetA;
			//printf("%d, %d, %d\n", int(y), input.size(3), int(x));
			output[y * input.size(3) + x] = rgbaToInt(r, g, b, a);
		}
		CUMAT_KERNEL_2D_LOOP_END
	}

	template<typename T>
	void CopyOutputToTextureImpl(
		int width, int height,
		const Tensor4Read<T>& output,
		GLubyte* texture,
		int r, int g, int b, int a,
		float scaleRGB, float offsetRGB, float scaleA, float offsetA,
		CUstream stream)
	{
		cuMat::Context& ctx = cuMat::Context::current();
		cuMat::KernelLaunchConfig cfg = ctx.createLaunchConfig2D(width, height, SelectOutputChannelKernel<T>);
		SelectOutputChannelKernel<T>
			<<<cfg.block_count, cfg.thread_per_block, 0, stream >>>
			(cfg.virtual_size, output, reinterpret_cast<unsigned*>(texture),
				r, g, b, a, scaleRGB, offsetRGB, scaleA, offsetA);
		CUMAT_CHECK_ERROR();
	}

	void CopyOutputToTexture(
		int width, int height,
		const Tensor4Read<float>& output,
		GLubyte* texture,
		int r, int g, int b, int a,
		float scaleRGB, float offsetRGB, float scaleA, float offsetA,
		CUstream stream)
	{
		CopyOutputToTextureImpl<float>(width, height, output, texture,
			r, g, b, a, scaleRGB, offsetRGB, scaleA, offsetA, stream);
	}
	void CopyOutputToTexture(
		int width, int height,
		const Tensor4Read<double>& output,
		GLubyte* texture,
		int r, int g, int b, int a,
		float scaleRGB, float offsetRGB, float scaleA, float offsetA,
		CUstream stream)
	{
		CopyOutputToTextureImpl<double>(width, height, output, texture,
			r, g, b, a, scaleRGB, offsetRGB, scaleA, offsetA, stream);
	}
}
