#include "module_registry.h"
#include "parameter.h"

#include "volume_interpolation.h"
#include "transfer_function.h"
#include "blending.h"
#include "camera.h"
#include "ray_evaluation_stepping.h"
#include "ray_evaluation_monte_carlo.h"
#include "image_evaluator.h"
#include "image_evaluator_neural_texture.h"
#include "brdf.h"

renderer::ModuleRegistry& renderer::ModuleRegistry::Instance()
{
	static ModuleRegistry INSTANCE;
	return INSTANCE;
}

void renderer::ModuleRegistry::registerModule(const std::string& tag, IModule_ptr module)
{
	map_[tag].push_back(module);
}

void renderer::ModuleRegistry::registerPybindModules(pybind11::module& m)
{
	//register parameters
	namespace py = pybind11;

#define PARAMETER_NON_DIFFERENTIABLE(T)										\
	py::class_<Parameter<T>>(m, "Parameter_" C10_STRINGIZE(T))				\
		.def_readwrite("value", &Parameter<T>::value)						\
		.def_property_readonly("supports_gradients", []()					\
			{return static_cast<bool>(Parameter<T>::supportsGradients); });
	PARAMETER_NON_DIFFERENTIABLE(bool);
	PARAMETER_NON_DIFFERENTIABLE(int);
	PARAMETER_NON_DIFFERENTIABLE(int2);
	PARAMETER_NON_DIFFERENTIABLE(int3);
	PARAMETER_NON_DIFFERENTIABLE(int4);
#undef PARAMETER_NON_DIFFERENTIABLE

#define PARAMETER_DIFFERENTIABLE(T)											\
	py::class_<Parameter<T>>(m, "Parameter_" C10_STRINGIZE(T))				\
		.def_readwrite("value", &Parameter<T>::value)						\
		.def_readwrite("grad", &Parameter<T>::grad)							\
		.def_readwrite("forwardIndex", &Parameter<T>::forwardIndex)			\
		.def_property_readonly("supports_gradients", []()					\
			{return static_cast<bool>(Parameter<T>::supportsGradients); });
	PARAMETER_DIFFERENTIABLE(double);
	PARAMETER_DIFFERENTIABLE(double2);
	PARAMETER_DIFFERENTIABLE(double3);
	PARAMETER_DIFFERENTIABLE(double4);
	using torch::Tensor;
	PARAMETER_DIFFERENTIABLE(Tensor);
#undef PARAMETER_DIFFERENTIABLE
	
	//register modules
	for (const auto& x : map_)
		for (const auto& v : x.second)
			v->registerPybindModule(m);
}

void renderer::ModuleRegistry::populateModules()
{
#define REGISTER(T)	\
	RegisterModule(std::make_shared<T>())

	REGISTER(VolumeInterpolationGrid);
	REGISTER(TransferFunctionIdentity);
	REGISTER(TransferFunctionTexture);
	REGISTER(TransferFunctionPiecewiseLinear);
	REGISTER(TransferFunctionGaussian);
	REGISTER(Blending);
	REGISTER(CameraOnASphere);
	REGISTER(RayEvaluationSteppingIso);
	REGISTER(RayEvaluationSteppingDvr);
	REGISTER(RayEvaluationMonteCarlo);
	REGISTER(ImageEvaluatorSimple);
	//REGISTER(ImageEvaluatorNeuralTexture);
	REGISTER(BRDFLambert);
}

const std::vector<renderer::IModule_ptr>& renderer::ModuleRegistry::getModulesForTag(const std::string& tag)
{
	auto it = map_.find(tag);
	TORCH_CHECK(it != map_.end(), "Tag ", tag, " not found!");
	return it->second;
}

void renderer::ModuleRegistry::loadAll(const nlohmann::json& root)
{
	for (const auto& x : map_)
	{
		const std::string& tag = x.first;
		if (!root.contains(tag))
		{
			std::cerr << "No object for tag '" << tag << "' found in the save file" << std::endl;
			continue;
		}
		const auto& tagObj = root[tag];
		for (const auto & v : x.second)
		{
			const std::string& name = v->getName();
			if (!tagObj.contains(name))
			{
				std::cerr << "No object for module '" << name << "' of tag '" << tag << "' found in the save file" << std::endl;
				continue;
			}
			v->load(tagObj[name]);
		}
	}
}

void renderer::ModuleRegistry::saveAll(nlohmann::json& root)
{
	for (const auto& x : map_)
	{
		const std::string& tag = x.first;
		nlohmann::json tagObj = nlohmann::json::object();
		for (const auto& v : x.second)
		{
			const std::string& name = v->getName();
			nlohmann::json moduleObj = nlohmann::json::object();
			v->save(moduleObj);
			tagObj[name] = moduleObj;
		}
		root[tag] = tagObj;
	}
}
