#pragma once

#include "renderer_tensor.cuh"
#include "renderer_utils.cuh"

// for intelli-sense

#ifndef RAY_EVALUATION_MONTE_CARLO__SAMPLING_T
#include "renderer_sampler_curand.cuh"
#define RAY_EVALUATION_MONTE_CARLO__SAMPLING_T ::kernel::Sampler
#endif

#ifndef RAY_EVALUATION_MONTE_CARLO__VOLUME_INTERPOLATION_T
#include "renderer_volume_grid.cuh"
#define RAY_EVALUATION_MONTE_CARLO__VOLUME_INTERPOLATION_T ::kernel::VolumeInterpolationGrid
#endif

#ifndef RAY_EVALUATION_MONTE_CARLO__TRANSFER_FUNCTION_T
#include "renderer_tf_gaussian.cuh"
#define RAY_EVALUATION_MONTE_CARLO__TRANSFER_FUNCTION_T ::kernel::TransferFunctionGaussian
#endif

#ifndef RAY_EVALUATION_MONTE_CARLO__BRDF_T
#include "renderer_brdf_lambert.cuh"
#define RAY_EVALUATION_MONTE_CARLO__BRDF_T ::kernel::BRDFLambert
#endif

namespace kernel
{
	struct RayEvaluationMonteCarloParameters
	{
		real_t maxAbsorption; //max absorption for delta tracking
		real_t densityMin;
		real_t densityMax;
		//fraction of the absorption in the transmittance,
		//that is responsible for scattering. 1-scatteringFactor
		//is then the emission
		real_t scatteringFactor;
	};
}

__constant__ ::kernel::RayEvaluationMonteCarloParameters rayEvaluationMonteCarloParameters;

namespace kernel
{
	struct RayEvaluationMonteCarlo
	{
		using VolumeInterpolation_t = RAY_EVALUATION_MONTE_CARLO__VOLUME_INTERPOLATION_T;
		using TF_t = RAY_EVALUATION_MONTE_CARLO__TRANSFER_FUNCTION_T;
		using BRDF_t = RAY_EVALUATION_MONTE_CARLO__BRDF_T;
		using Sampler_t = RAY_EVALUATION_MONTE_CARLO__SAMPLING_T;

		VolumeInterpolation_t volume;
		TF_t tf;
		BRDF_t brdf;

		/**
		 * Background intersection. No scattering in the medium happened,
		 * now check for area lights.
		 */
		template<typename Sampler = Sampler_t>
		__device__ __inline__
		RayEvaluationOutput evalBackground(real3 rayStart, real3 rayDir, real_t t, int batch, Sampler& sampler)
		{
			//TODO: implement background lights
			return RayEvaluationOutput{
				make_real4(0,0,0,0),
				make_real3(0,0,0),
				t
			};
		}

		/**
		 * A scattering event happened at that time
		 */
		template<typename Sampler = Sampler_t>
		__device__ __inline__
			RayEvaluationOutput evalScattering(
				real3 position, real3 rayDir, real_t tcurrent, real3 gradient, real4 tfColor, int batch, Sampler& sampler)
		{
			//emission
			auto colorEmission = make_real3(brdf.eval(tfColor, position, gradient, rayDir, batch));

			//scattering
			auto colorScattering = make_real3(0, 0, 0); //TODO
			
			//output
			real_t f = rayEvaluationMonteCarloParameters.scatteringFactor;
			return RayEvaluationOutput{
				make_real4((1-f) * colorEmission, 1),
				safeNormalize(gradient),
				tcurrent
			};
		}
		
		template<typename Sampler = Sampler_t>
		__device__ __inline__
		RayEvaluationOutput eval(real3 rayStart, real3 rayDir, real_t tmax, int batch, Sampler& sampler)
		{
			real_t tmin, tmax1;
			intersectionRayAABB(
				rayStart, rayDir, volume.getBoxMin(), volume.getBoxSize(),
				tmin, tmax1);
			tmin = rmax(tmin, 0);
			tmax = rmin(tmax1, tmax);

			//stepping
			const real_t maxAbsorption = rayEvaluationMonteCarloParameters.maxAbsorption;
			const real_t divMaxAbsorption = 1 / maxAbsorption;
			const real_t densityMin = rayEvaluationMonteCarloParameters.densityMin;
			const real_t densityMax = rayEvaluationMonteCarloParameters.densityMax;
			const real_t divDensityRange = real_t(1) / (densityMax - densityMin);

			//delta-tracking
			real_t tcurrent = tmin;
			while (true)
			{
				//sample in homogeneous medium
				real_t negStepsize = rlog(sampler.sampleUniform()) * divMaxAbsorption;
				tcurrent -= negStepsize;
				if (tcurrent >= tmax)
					return evalBackground(rayStart, rayDir, tmax, batch, sampler);
				//evaluate current density
				real3 position = rayStart + rayDir * tcurrent;
				const auto [density, isInside] = volume.eval(position, batch);
				//if (threadIdx.x == 0 && blockIdx.x == 0)
				//	printf("t=%.4f, pos=(%.3f,%.3f,%.3f) -> d=%.4f\n",
				//		tcurrent, position.x, position.y, position.z, density);
				if (density >= densityMin)
				{
					auto density2 = (density - densityMin) * divDensityRange;

					//evaluate TF
					VolumeInterpolation_t::density_t previousDensity = { 0 };
					auto n = volume.evalNormal(position, batch);
					auto color1 = tf.eval(density2, n, previousDensity, 1, batch);
					auto currentAbsorption = color1.w;

					//if (threadIdx.x == 0 && blockIdx.x == 0)
					//	printf("color: %.3f,%.3f,%.3f,%.3f\n",
					//		color1.x, color1.y, color1.z, color1.w);
					
					//check if this was a virtual particle or real particle
					if (currentAbsorption * divMaxAbsorption > sampler.sampleUniform())
					{
						//real particle
						return evalScattering(position, rayDir, tcurrent, n, color1, batch, sampler);
					}
				}
			}
		}
	};
}