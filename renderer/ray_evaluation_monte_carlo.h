#pragma once

#include "ray_evaluation.h"

BEGIN_RENDERER_NAMESPACE

class RayEvaluationMonteCarlo : public IRayEvaluation
{
protected:
	double minDensity_;
	double maxDensity_;
	double scatteringFactor_; //TODO: make Parameter<double>
	
	ITransferFunction_ptr tf_;
	IBRDF_ptr brdf_;
	//TODO: Phase Function

public:
	RayEvaluationMonteCarlo();

	bool requiresSampler() override { return true; }
	bool shouldSupersample() override { return true; }
	
	[[nodiscard]] ITransferFunction_ptr getSelectedTF() const { return tf_; }
	[[nodiscard]] IBRDF_ptr getSelectedBRDF() const { return brdf_; }

	std::string getName() const override;
	void prepareRendering(GlobalSettings& s) const override;
	std::string getDefines(const GlobalSettings& s) const override;
	std::vector<std::string> getIncludeFileNames(const GlobalSettings& s) const override;
	std::string getConstantDeclarationName(const GlobalSettings& s) const override;
	std::string getPerThreadType(const GlobalSettings& s) const override;
	void fillConstantMemory(const GlobalSettings& s, CUdeviceptr ptr, CUstream stream) override;
	bool drawUI(UIStorage_t& storage) override;
	IModule_ptr getSelectedModuleForTag(const std::string& tag) override;
	std::vector<std::string> getSupportedTags() override;
	void load(const nlohmann::json& json) override;
	void save(nlohmann::json& json) const override;
protected:
	void registerPybindModule(pybind11::module& m) override;
};

END_RENDERER_NAMESPACE
