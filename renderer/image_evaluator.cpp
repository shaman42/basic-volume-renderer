#include "image_evaluator.h"

#include <sstream>
#include <magic_enum.hpp>
#include <torch/csrc/cuda/Stream.h>
#include <cuMat/src/Context.h>

#include "tinyformat.h"
#include "pytorch_utils.h"
#include "module_registry.h"

const char* renderer::IImageEvaluator::ChannelModeNames[] = {
	"Mask", "Normal", "Depth", "Color"
};

const std::string renderer::IImageEvaluator::UI_KEY_SELECTED_CAMERA = "camera::IImageEvaluator";
const std::string renderer::IImageEvaluator::UI_KEY_SELECTED_OUTPUT_CHANNEL = "outputChannel::IImageEvaluator";
const std::string renderer::IImageEvaluator::UI_KEY_USE_DOUBLE_PRECISION = "doublePrecision::IImageEvaluator";

renderer::IImageEvaluator::IImageEvaluator()
	: selectedChannel_(ChannelMode::ChannelColor)
	, isDoublePrecision_(false)
{
}

void renderer::IImageEvaluator::copyOutputToTexture(
	const torch::Tensor& output, GLubyte* texture, ChannelMode channel, CUstream stream)
{
	CHECK_CUDA(output, true);
	CHECK_DIM(output, 4);
	CHECK_SIZE(output, 1, 8);
	CHECK_SIZE(output, 0, 1); //only one batch
	int height = output.size(2);
	int width = output.size(3);

	int r, g, b, a;
	float scaleRGB, offsetRGB, scaleA, offsetA;
	switch (channel)
	{
	case ChannelMode::ChannelColor:
	{
		r = 0; g = 1; b = 2; a = 3;
		scaleRGB = 1; offsetRGB = 0;
		scaleA = 1; offsetA = 0;
	} break;
	case ChannelMode::ChannelDepth:
	{
		//TODO: only select pixels that contain data
		float minDepth = output.select(1, 7).min().item().toFloat();
		float maxDepth = output.select(1, 7).max().item().toFloat();
		r = g = b = 7; a = 3;
		scaleRGB = 1 / (maxDepth - minDepth);
		offsetRGB = -minDepth / (maxDepth - minDepth);
		scaleA = 1; offsetA = 0;
	} break;
	case ChannelMode::ChannelMask:
	{
		r = g = b = a = 3;
		scaleRGB = 1; offsetRGB = 0;
		scaleA = 0; offsetA = 1;
	} break;
	case ChannelMode::ChannelNormal:
		r = 4; g = 5; b = 6; a = 3;
		scaleRGB = 0.5; offsetRGB = 0.5;
		scaleA = 1; offsetA = 0;
	}
	
	RENDERER_DISPATCH_FLOATING_TYPES(output.scalar_type(), "IImageEvaluator::copyOutputToTexture", [&]()
		{
			const auto acc = accessor < ::kernel::Tensor4Read<scalar_t>>(output);
			::kernel::CopyOutputToTexture(
				width, height, acc, texture, 
				r, g, b, a, scaleRGB, offsetRGB, scaleA, offsetA, 
				stream);
		});
}

int renderer::IImageEvaluator::computeBatchCount()
{
	const auto s = getGlobalSettings();
	int batch = 1;
	std::string lastBatchModule;
	for (const auto& tag : getSupportedTags())
	{
		const auto m = getSelectedModuleForTag(tag);
		if (IKernelModule_ptr km = std::dynamic_pointer_cast<IKernelModule>(m))
		{
			auto b = km->getBatches(s).value_or(1);
			if (b > 1)
			{
				std::string currentBatchModule = km->getTag() + ":" + km->getName();
				if (batch>1 && batch!=b)
				{
					throw std::runtime_error(tinyformat::format(
						"Batch counts don't match. Module %s has %d batches, but the current module %s has %d batches",
						lastBatchModule, batch, currentBatchModule, b
					));
				}
				lastBatchModule = currentBatchModule;
				batch = b;
			}
		}
	}
	return batch;
}

CUstream renderer::IImageEvaluator::getDefaultStream()
{
	return torch::cuda::getCurrentCUDAStream();
}

renderer::IKernelModule::GlobalSettings renderer::IImageEvaluator::getGlobalSettings(
	const std::optional<bool>& useDoublePrecision)
{
	renderer::IKernelModule::GlobalSettings s;
	s.root = shared_from_this();
	s.scalarType = useDoublePrecision.value_or(isDoublePrecision_)
		? c10::kDouble : c10::kFloat;
	return s;
}

void renderer::IImageEvaluator::modulesPrepareRendering(IKernelModule::GlobalSettings& s)
{
	const auto oldScalarType = s.scalarType;
	for (const auto& tag : getSupportedTags())
	{
		const auto m = getSelectedModuleForTag(tag);
		if (IKernelModule_ptr km = std::dynamic_pointer_cast<IKernelModule>(m))
		{
			km->prepareRendering(s);
		}
	}
	TORCH_CHECK(oldScalarType == s.scalarType, "the scalar type must not be changed!");
}

renderer::KernelLoader::KernelFunction renderer::IImageEvaluator::getKernel(
	const IKernelModule::GlobalSettings& s, const std::string& kernelName, const std::string& extraSource)
{
	std::stringstream defines;
	std::stringstream includes;
	std::vector<std::string> constantNames;

	defines << "#define KERNEL_DOUBLE_PRECISION "
		<< (s.scalarType == IKernelModule::GlobalSettings::kDouble ? 1 : 0)
		<< "\n";
	
	for (const auto& tag : getSupportedTags())
	{
		const auto m = getSelectedModuleForTag(tag);
		if (IKernelModule_ptr km = std::dynamic_pointer_cast<IKernelModule>(m))
		{
			defines <<
				"// " << km->getTag() << " : " << km->getName() << "\n"
				<< km->getDefines(s) << "\n";
			includes <<
				"// " << km->getTag() << " : " << km->getName() << "\n";
			for (const auto& i : km->getIncludeFileNames(s))
				includes << "#include \"" << i << "\"\n";
			const auto c = km->getConstantDeclarationName(s);
			if (!c.empty())
				constantNames.push_back(c);
		}
	}

	std::stringstream sourceFile;
	sourceFile << "// DEFINES:\n" << defines.str()
		<< "\n// INCLUDES:\n" << includes.str()
		<< "\n// MAIN SOURCE:\n" << extraSource;

	//create kernel
	const auto& fun = KernelLoader::Instance().getKernelFunction(
		kernelName, sourceFile.str(), constantNames, false, false);
	return fun.value();
}

void renderer::IImageEvaluator::fillConstants(
	KernelLoader::KernelFunction& f, const IKernelModule::GlobalSettings& s, CUstream stream)
{
	for (const auto& tag : getSupportedTags())
	{
		const auto m = getSelectedModuleForTag(tag);
		if (IKernelModule_ptr km = std::dynamic_pointer_cast<IKernelModule>(m))
		{
			const auto c = km->getConstantDeclarationName(s);
			if (!c.empty())
			{
				CUdeviceptr ptr = f.constant(c);
				km->fillConstantMemory(s, ptr, stream);
			}
		}
	}
}

bool renderer::IImageEvaluator::drawUIGlobalSettings(UIStorage_t& storage)
{
	//double precision is shared
	bool isDoublePrecision = isDoublePrecision_;
	if (const auto& it = storage.find(UI_KEY_USE_DOUBLE_PRECISION);
		it != storage.end())
	{
		isDoublePrecision = std::any_cast<bool>(it->second);
	}
	
	bool changed = false;
	if (ImGui::Checkbox("double precision##IImageEvaluator", &isDoublePrecision))
		changed = true;

	isDoublePrecision_ = isDoublePrecision;
	storage[UI_KEY_USE_DOUBLE_PRECISION] = isDoublePrecision;
	
	return changed;
}

bool renderer::IImageEvaluator::drawUIOutputChannel(UIStorage_t& storage, bool readOnly)
{
	//selected channel is shared
	ChannelMode selectedChannel = selectedChannel_;
	if (const auto& it = storage.find(UI_KEY_SELECTED_OUTPUT_CHANNEL);
		it != storage.end())
	{
		selectedChannel = std::any_cast<ChannelMode>(it->second);
	}
	
	bool changed = false;
	const char* currentChannelName = ChannelModeNames[int(selectedChannel)];
	if (readOnly)
	{
		int tmp = static_cast<int>(selectedChannel);
		ImGui::SliderInt("Channel", &tmp,
			0, int(ChannelMode::_ChannelCount_) - 1, currentChannelName);
	}
	else {
		if (ImGui::SliderInt("Channel", reinterpret_cast<int*>(&selectedChannel),
			0, int(ChannelMode::_ChannelCount_) - 1, currentChannelName))
			changed = true;
	}

	selectedChannel_ = selectedChannel;
	storage[UI_KEY_SELECTED_OUTPUT_CHANNEL] = selectedChannel;
	
	return changed;
}


void renderer::IImageEvaluator::load(const nlohmann::json& json)
{
	selectedChannel_ = magic_enum::enum_cast<ChannelMode>(json.value("outputChannel", "")).
		value_or(ChannelMode::ChannelColor);
	isDoublePrecision_ = json.value("doublePrecision", false);
}

void renderer::IImageEvaluator::save(nlohmann::json& json) const
{
	json["outputChannel"] = magic_enum::enum_name(selectedChannel_);
	json["doublePrecision"] = isDoublePrecision_;
}

void renderer::IImageEvaluator::registerPybindModule(pybind11::module& m)
{
	//guard double registration
	static bool registered = false;
	if (registered) return;
	registered = true;
	
	namespace py = pybind11;
	py::class_<IImageEvaluator, IImageEvaluator_ptr> c(m, "IImageEvaluator");
	py::enum_<ChannelMode>(c, "ChannelMode")
		.value("Mask", ChannelMode::ChannelMask)
		.value("Normal", ChannelMode::ChannelNormal)
		.value("Depth", ChannelMode::ChannelDepth)
		.value("Color", ChannelMode::ChannelColor)
		.export_values();
	c.def_readwrite("selected_channel", &IImageEvaluator::selectedChannel_)
		.def_readwrite("double_precision", &IImageEvaluator::isDoublePrecision_)
		.def("render", [this](int width, int height)
			{
				return render(width, height, getDefaultStream());
			},
			py::doc("Renders the image at the given resolution of width*height"))
		.def("compute_batch_count", &IImageEvaluator::computeBatchCount)
		.def("get_supported_tags", &IImageEvaluator::getSupportedTags)
		.def("get_module_for_tag", &IImageEvaluator::getSelectedModuleForTag);
}


renderer::ImageEvaluatorSimple::ImageEvaluatorSimple()
	: isUImode_(false)
	, samplesPerIterationLog2_(3)
	, currentTime_(0)
{
}

std::string renderer::ImageEvaluatorSimple::getName() const
{
	return "Simple";
}

bool renderer::ImageEvaluatorSimple::drawUI(UIStorage_t& storage)
{
	isUImode_ = true;
	bool changed = false;
	if (drawUIGlobalSettings(storage))
		changed = true;

	//camera
	ICamera_ptr camera = selectedCamera_;
	if (const auto& it = storage.find(UI_KEY_SELECTED_CAMERA);
		it != storage.end())
	{
		camera = std::any_cast<ICamera_ptr>(it->second);
	}
	const auto& cameras =
		ModuleRegistry::Instance().getModulesForTag(ICamera::Tag());
	if (camera == nullptr)
		camera = std::dynamic_pointer_cast<ICamera>(cameras[0]);
	if (ImGui::CollapsingHeader("Camera##IImageEvaluator", ImGuiTreeNodeFlags_DefaultOpen))
	{
		if (cameras.size() > 1) {
			for (int i = 0; i < cameras.size(); ++i) {
				const auto& name = cameras[i]->getName();
				if (ImGui::RadioButton(name.c_str(), cameras[i] == camera)) {
					camera = std::dynamic_pointer_cast<ICamera>(cameras[i]);
					changed = true;
				}
				if (i < cameras.size() - 1) ImGui::SameLine();
			}
		}
		if (camera->drawUI(storage))
			changed = true;
	}
	selectedCamera_ = camera;
	storage[UI_KEY_SELECTED_CAMERA] = camera;

	//ray evaluator (not shared)
	const auto& rayEvaluators =
		ModuleRegistry::Instance().getModulesForTag(IRayEvaluation::Tag());
	if (selectedRayEvaluator_ == nullptr)
		selectedRayEvaluator_ = std::dynamic_pointer_cast<IRayEvaluation>(rayEvaluators[0]);
	if (rayEvaluators.size() > 1)
	{
		for (int i = 0; i < rayEvaluators.size(); ++i) {
			const auto& name = rayEvaluators[i]->getName();
			if (ImGui::RadioButton(name.c_str(), rayEvaluators[i] == selectedRayEvaluator_)) {
				selectedRayEvaluator_ = std::dynamic_pointer_cast<IRayEvaluation>(rayEvaluators[i]);
				changed = true;
			}
			if (i < rayEvaluators.size() - 1) ImGui::SameLine();
		}
	}
	if (selectedRayEvaluator_->shouldSupersample()) {
		std::string currentSamples = std::to_string(1 << samplesPerIterationLog2_);
		if (ImGui::SliderInt("SPP##ImageEvaluatorSimple", &samplesPerIterationLog2_, 0, 6, currentSamples.c_str()))
			changed = true;
	}
	if (selectedRayEvaluator_->drawUI(storage))
		changed = true;
	
	if (drawUIOutputChannel(storage))
		changed = true;
	return changed;
}

renderer::IModule_ptr renderer::ImageEvaluatorSimple::getSelectedModuleForTag(const std::string& tag)
{
	if (tag == ICamera::Tag())
		return selectedCamera_;
	if (tag == IRayEvaluation::Tag())
		return selectedRayEvaluator_;
	return selectedRayEvaluator_->getSelectedModuleForTag(tag);
}

std::vector<std::string> renderer::ImageEvaluatorSimple::getSupportedTags()
{
	std::vector<std::string> tags;
	tags.push_back(ICamera::Tag());
	const auto& t = selectedRayEvaluator_->getSupportedTags();
	tags.insert(tags.end(), t.begin(), t.end());
	tags.push_back(IRayEvaluation::Tag());
	return tags;
}

torch::Tensor renderer::ImageEvaluatorSimple::render(int width, int height, CUstream stream, const torch::Tensor& out)
{
	//settings
	selectedCamera_->setAspectRatio(double(width) / height);
	IKernelModule::GlobalSettings s = getGlobalSettings();
	if (!isUImode_ || selectedChannel_ == ChannelMode::ChannelNormal)
		s.volumeShouldProvideNormals = true;
	modulesPrepareRendering(s);
	const std::string kernelName = "ImageEvaluatorSimpleKernel";
	std::stringstream extraSource;
	extraSource << "#define IMAGE_EVALUATOR__CAMERA_T " <<
		selectedCamera_->getPerThreadType(s) << "\n";
	extraSource << "#define IMAGE_EVALUATOR__RAY_EVALUATOR_T " <<
		selectedRayEvaluator_->getPerThreadType(s) << "\n";
	int numSamples = 1;
	if (selectedRayEvaluator_->requiresSampler())
		extraSource << "#define IMAGE_EVALUATOR__REQUIRES_SAMPLER\n";
	if (selectedRayEvaluator_->shouldSupersample())
	{
		numSamples = 1 << samplesPerIterationLog2_;
		extraSource << "#define IMAGE_EVALUATOR__SUPERSAMPLING\n";
	}
	extraSource << "#include \"renderer_image_evaluator_simple.cuh\"\n";
	KernelLoader::KernelFunction fun = getKernel(
		s, kernelName, extraSource.str());
	fillConstants(fun, s, stream);

	int batches = computeBatchCount();
	
	//allocate tensor
	torch::Tensor t = out;
	if (t.scalar_type() != s.scalarType ||
		!t.is_cuda() ||
		t.ndimension() != 4 ||
		t.size(0) != batches ||
		t.size(1) != NUM_CHANNELS ||
		t.size(2) != height ||
		t.size(3) != width)
	{
		//re-allocate
		t = torch::empty({ batches, NUM_CHANNELS, height, width },
			at::TensorOptions().dtype(s.scalarType).device(c10::kCUDA));
	}
	
	//launch kernel
	int minGridSize = std::min(
		int(CUMAT_DIV_UP(width * height * batches, fun.bestBlockSize())),
		fun.minGridSize());
	dim3 virtual_size{
		static_cast<unsigned int>(width),
		static_cast<unsigned int>(height),
		static_cast<unsigned int>(batches) };
	bool success = RENDERER_DISPATCH_FLOATING_TYPES(s.scalarType, "ImageEvaluatorSimple::render", [&]()
	{
		const auto acc = accessor< ::kernel::Tensor4Read<scalar_t>>(t);
		const void* args[] = { &virtual_size, &acc, &numSamples, &currentTime_ };
		auto result = cuLaunchKernel(
			fun.fun(), minGridSize, 1, 1, fun.bestBlockSize(), 1, 1,
			0, stream, const_cast<void**>(args), NULL);
		if (result != CUDA_SUCCESS)
			return printError(result, kernelName);
		return true;
	});
	currentTime_++;
	if (!success) throw std::runtime_error("Error during rendering!");
	return t;
}

void renderer::ImageEvaluatorSimple::load(const nlohmann::json& json)
{
	std::string cameraName = json.value("selectedCamera", "");
	const auto& cameras =
		ModuleRegistry::Instance().getModulesForTag(ICamera::Tag());
	for (const auto v : cameras)
	{
		if (v->getName() == cameraName) {
			selectedCamera_ = std::dynamic_pointer_cast<ICamera>(v);
			break;
		}
	}

	std::string rayEvaluatorName = json.value("selectedRayEvaluator", "");
	const auto& rayEvaluators =
		ModuleRegistry::Instance().getModulesForTag(IRayEvaluation::Tag());
	for (const auto v : rayEvaluators)
	{
		if (v->getName() == rayEvaluatorName) {
			selectedRayEvaluator_ = std::dynamic_pointer_cast<IRayEvaluation>(v);
			break;
		}
	}

	samplesPerIterationLog2_ = json.value("samplesPerIterationLog2", 2);
}

void renderer::ImageEvaluatorSimple::save(nlohmann::json& json) const
{
	json["selectedCamera"] = selectedCamera_->getName();
	json["selectedRayEvaluator"] = selectedRayEvaluator_->getName();
	json["samplesPerIterationLog2"] = samplesPerIterationLog2_;
}

void renderer::ImageEvaluatorSimple::registerPybindModule(pybind11::module& m)
{
	IImageEvaluator::registerPybindModule(m);

	//guard double registration
	static bool registered = false;
	if (registered) return;
	registered = true;
	
	namespace py = pybind11;
	py::class_<ImageEvaluatorSimple, IImageEvaluator, std::shared_ptr<ImageEvaluatorSimple>>(m, "ImageEvaluatorSimple")
		.def_readwrite("camera", &ImageEvaluatorSimple::selectedCamera_,
			py::doc("The selected ICamera method"))
		.def_readwrite("ray_evaluator", &ImageEvaluatorSimple::selectedRayEvaluator_,
			py::doc("The selected IRayEvaluation method"));
}
