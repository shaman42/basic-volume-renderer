#pragma once

/**
 * Neural texture / hologram ImageEvaluator:
 * A textured bounding object is rendered and the sampled texels
 * are interpreted by the network to give rise to the final color.
 *
 * The texture interpolation is done manually, the network
 * is evaluated using TorchScript.
 *
 * Texture format: BxCxHxW of type float32 or float64 where
 *  - B: the batch count (1 for sphere or octahedron, 6 for cube)
 *  - C: the number of channels, must be a power of four
 *  - H,W: height and width of the texture
 *
 * Network format: a function y=f(x) where
 *  - x=[dir,channels] in (B,C) with dir=ray direction in world space,
 *     channels are the sampled texture channel [frontface,backface,length]
 *  - y in (B,4) the output rgb-alpha
 */

#include <json.hpp>
#include <torch/script.h>
#include <stdint.h>
#include "image_evaluator.h"

BEGIN_RENDERER_NAMESPACE

class ImageEvaluatorNeuralTexture;

/**
 * Serializable description of the neural texture model.
 * Stores the texture, the network and flags for evaluation.
 *
 * Save file format:
 * 
 * <Header, 32 Bytes>
 * 0-3: magic number
 * 4-7: version
 * 8-11: flags
 * 12-15: bounding object radius (float)
 * 16-19: texture batches B
 * 20-23: texture channels C
 * 24-27: texture height H
 * 28-31: texture width W
 * 32-35: network storage size in bytes N
 * 36-63: unused
 *
 * <Texture>
 * Blob of float32 with B*C*H*W entries, indexed in that order.
 * Batch B slowest, width W fastest
 *
 * <Network>
 * Blob of N bytes
 */
class NeuralTextureModel
{
public:
	static constexpr int8_t MAGIC[4] = { 'N', 'T', 'M', 'x' };
	static constexpr int HEADER_LENGTH = 64;
	static constexpr int32_t VERSION = 1;
	static const std::string PREFERRED_EXTENSION;

	enum Flags : int32_t
	{
		Flag_BoundingBox = 0x1,
		Flag_BoundingSphere = 0x2,
		Flag_BoundingOctahedron = 0x3,

		//The backface 
		Flag_HasBackface = 0x10,
		Flag_HasLength = 0x20,
	};

private:
	int32_t flags_;
	float radius_;
	torch::Tensor texture_;
	torch::jit::Module network_;

public:
	NeuralTextureModel(int32_t flags, float radius, const torch::Tensor& texture,
		const torch::jit::Module& network);

public:
	static std::shared_ptr<NeuralTextureModel> load(const std::string& filename);

	void save(const std::string& filename) const;

	void toCuda();

private:
	friend class ImageEvaluatorNeuralTexture;
	static void registerPybindModule(pybind11::module& m);
};
typedef std::shared_ptr<NeuralTextureModel> NeuralTextureModel_ptr;

class ImageEvaluatorNeuralTexture : public IImageEvaluator
{
	ICamera_ptr selectedCamera_;
	NeuralTextureModel_ptr selectedModel_;

	//UI
	struct LoadedModel
	{
		NeuralTextureModel_ptr model;
		std::string fullFilename;
		std::string shortFilename;
	};
	std::vector<LoadedModel> loadedModels_;
	int selectedModelIndex_ = -1;
	
public:
	[[nodiscard]] std::string getName() const override;
	[[nodiscard]] bool drawUI(UIStorage_t& storage) override;
	[[nodiscard]] IModule_ptr getSelectedModuleForTag(const std::string& tag) override;
	[[nodiscard]] std::vector<std::string> getSupportedTags() override;
	torch::Tensor render(int width, int height, CUstream stream, const torch::Tensor& out) override;
	void load(const nlohmann::json& json) override;
	void save(nlohmann::json& json) const override;

	ICamera_ptr selectedCamera() const { return selectedCamera_; }
	void setSelectedCamera(ICamera_ptr cam) { selectedCamera_ = cam; }
	NeuralTextureModel_ptr selectedModel() const { return selectedModel_; }
	void setSelectedModel(NeuralTextureModel_ptr model)
	{
		selectedModel_ = model;
		selectedModelIndex_ = -1;
	}

protected:
	void registerPybindModule(pybind11::module& m) override;

private:
	void loadModelUI();
	std::optional<LoadedModel> loadModel(const std::string& fullFilename);
};

END_RENDERER_NAMESPACE
