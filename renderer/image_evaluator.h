#pragma once

#include <GL/glew.h>
#include <torch/types.h>
#include <cuda.h>

#include "imodule.h"
#include "kernel_loader.h"
#include "camera.h"
#include "ray_evaluation.h"
#include "renderer_tensor.cuh"

BEGIN_RENDERER_NAMESPACE

/**
 * The main entry point
 */
class IImageEvaluator : public IModuleContainer, public std::enable_shared_from_this<IImageEvaluator>
{
public:
	enum class ChannelMode
	{
		ChannelMask,
		ChannelNormal,
		ChannelDepth,
		ChannelColor,
		_ChannelCount_
	};
	static const char* ChannelModeNames[int(ChannelMode::_ChannelCount_)];

protected:
	//ui
	static const std::string UI_KEY_SELECTED_CAMERA;
	static const std::string UI_KEY_SELECTED_OUTPUT_CHANNEL;
	ChannelMode selectedChannel_;
	static const std::string UI_KEY_USE_DOUBLE_PRECISION;
	bool isDoublePrecision_;

	IImageEvaluator();

public:
	static constexpr int NUM_CHANNELS = 8;
	/**
	 * Renders the image with the given screen size.
	 * This has to be overwritten by the implementations
	 * \param width the width of the screen
	 * \param height the height of the screen
	 * \param out output tensor to reuse
	 * \return a BCHW tensor with the channels being
	 *  0,1,2: rgb
	 *  3: alpha
	 *  4,5,6: normal
	 *  7: depth
	 */
	virtual torch::Tensor render(
		int width, int height, CUstream stream, const torch::Tensor& out = {}) = 0;
	
	/**
	 * Copies the output from \ref render() to the specified
	 * texture of type RGBA8 for display. 
	 */
	virtual void copyOutputToTexture(
		const torch::Tensor& output, GLubyte* texture,
		ChannelMode channel, CUstream stream);

	[[nodiscard]] virtual ChannelMode selectedChannel() const
	{
		return selectedChannel_;
	}

	virtual void setSelectedChannel(const ChannelMode selectedChannel)
	{
		selectedChannel_ = selectedChannel;
	}

	[[nodiscard]] virtual bool isDoublePrecision() const
	{
		return isDoublePrecision_;
	}

	virtual void setIsDoublePrecision(const bool isDoublePrecision)
	{
		isDoublePrecision_ = isDoublePrecision;
	}

	/**
	 * The batch count is specified by the contained submodules.
	 * This functions computes the batch count and verifies that they
	 * are compatible. This means, if one module has a batch size > 1,
	 * all modules with a batch size > 1 must agree to that batch size.
	 *
	 * This calls \ref IKernelModule::getBatches()
	 */
	int computeBatchCount();

	/**
	 * Returns the default stream that is
	 * shared with PyTorch.
	 */
	static CUstream getDefaultStream();

protected:
	/**
	 * Creates the global settings for the kernel assembly.
	 * In detail, sets the scalar type and the root module to this.
	 * \param useDoublePrecision true iff double precision should be used.
	 *  If empty, the setting from the UI is used.
	 */
	IKernelModule::GlobalSettings getGlobalSettings(
		const std::optional<bool>& useDoublePrecision = {});

	/**
	 * Calls \ref IKernelModule::prepareRendering of the kernel modules.
	 * This also allows them to modify the settings.
	 */
	void modulesPrepareRendering(IKernelModule::GlobalSettings& s);
	
	/**
	 * Queries the submodules and compiles the main rendering kernel.
	 * This calls \ref IKernelModule::getDefines(),
	 * \ref IKernelModule::getIncludeFileNames,
	 * \ref IKernelModule::getConstantDeclarationName()
	 */
	KernelLoader::KernelFunction getKernel(
		const IKernelModule::GlobalSettings& s, 
		const std::string& kernelName,
		const std::string& extraSource);

	/**
	 * Queries the submodules and fills the constants
	 */
	void fillConstants(
		KernelLoader::KernelFunction& f, const IKernelModule::GlobalSettings& s,
		CUstream stream);

	/**
	 * Draws UI for the global settings (i.e. the scalar type).
	 * This should be on top of the UI.
	 */
	bool drawUIGlobalSettings(UIStorage_t& s);

	/**
	 * Draws the UI for the output channel.
	 * This should be at the bottom of the UI
	 */
	bool drawUIOutputChannel(UIStorage_t& s, bool readOnly=false);

public:
	static constexpr std::string_view TAG = "ImageEvaluator";
	static std::string Tag() { return std::string(TAG); }
	std::string getTag() const override { return Tag(); }

	void load(const nlohmann::json& json) override;
	void save(nlohmann::json& json) const override;
protected:
	void registerPybindModule(pybind11::module& m) override;
};
typedef std::shared_ptr<IImageEvaluator> IImageEvaluator_ptr;

/**
 * Simple image evaluator that stores a camera and ray evaluator.
 * No super- or subsampling, no postprocessing like SR networks or
 * tonemapping.
 */
class ImageEvaluatorSimple : public IImageEvaluator
{
	ICamera_ptr selectedCamera_;
	IRayEvaluation_ptr selectedRayEvaluator_;
	bool isUImode_;

	int samplesPerIterationLog2_;
	unsigned int currentTime_;

public:
	ImageEvaluatorSimple();

	std::string getName() const override;
	bool drawUI(UIStorage_t& storage) override;
	IModule_ptr getSelectedModuleForTag(const std::string& tag) override;
	std::vector<std::string> getSupportedTags() override;
	torch::Tensor render(int width, int height, CUstream stream, const torch::Tensor& out) override;
	void load(const nlohmann::json& json) override;
	void save(nlohmann::json& json) const override;
protected:
	void registerPybindModule(pybind11::module& m) override;
};


END_RENDERER_NAMESPACE

namespace kernel
{
	void CopyOutputToTexture(
		int width, int height,
		const Tensor4Read<float>& output,
		GLubyte* texture,
		int r, int g, int b, int a,
		float scaleRGB, float offsetRGB, float scaleA, float offsetA,
		CUstream stream);
	void CopyOutputToTexture(
		int width, int height,
		const Tensor4Read<double>& output,
		GLubyte* texture,
		int r, int g, int b, int a,
		float scaleRGB, float offsetRGB, float scaleA, float offsetA,
		CUstream stream);
}
