#include "ray_evaluation.h"

#include "module_registry.h"

renderer::IRayEvaluation::IRayEvaluation()
	: selectedVolume_(0)
{
}

const std::string renderer::IRayEvaluation::UI_KEY_SELECTED_VOLUME = "IRayEvaluation::SelectedVolume";
const std::string renderer::IRayEvaluation::UI_KEY_SELECTED_MIN_DENSITY = "IRayEvaluation::MinDensity";
const std::string renderer::IRayEvaluation::UI_KEY_SELECTED_MAX_DENSITY = "IRayEvaluation::MaxDensity";
bool renderer::IRayEvaluation::drawUI(UIStorage_t& storage)
{
	//volume selection is shared
	IVolumeInterpolation_ptr selection = selectedVolume_;
	if (const auto& it = storage.find(UI_KEY_SELECTED_VOLUME);
		it != storage.end())
	{
		selection = std::any_cast<IVolumeInterpolation_ptr>(it->second);
	}

	bool changed = false;
	ImGui::PushID("IRayEvaluation");
	const auto& volumes = 
		ModuleRegistry::Instance().getModulesForTag(IVolumeInterpolation::Tag());
	if (selection == nullptr)
		selection = std::dynamic_pointer_cast<IVolumeInterpolation>(volumes[0]);
	if (ImGui::CollapsingHeader("Volume", ImGuiTreeNodeFlags_DefaultOpen))
	{
		for (int i = 0; i < volumes.size(); ++i) {
			const auto& name = volumes[i]->getName();
			if (ImGui::RadioButton(name.c_str(), volumes[i] == selection)) {
				selection = std::dynamic_pointer_cast<IVolumeInterpolation>(volumes[i]);
				changed = true;
			}
			if (i < volumes.size()-1) ImGui::SameLine();
		}
		if (selection->drawUI(storage))
			changed = true;
	}
	ImGui::PopID();
	
	storage[UI_KEY_SELECTED_VOLUME] = static_cast<IVolumeInterpolation_ptr>(selection);
	selectedVolume_ = selection;
	return changed;
}

void renderer::IRayEvaluation::load(const nlohmann::json& json)
{
	std::string selectionName = json.value("selectedVolume", "");
	const auto& volumes =
		ModuleRegistry::Instance().getModulesForTag(IVolumeInterpolation::Tag());
	for (const auto v : volumes)
	{
		if (v->getName() == selectionName) {
			selectedVolume_ = std::dynamic_pointer_cast<IVolumeInterpolation>(v);
			break;
		}
	}
}

void renderer::IRayEvaluation::save(nlohmann::json& json) const
{
	json["selectedVolume"] = selectedVolume_ ? selectedVolume_->getName() : "";
}

void renderer::IRayEvaluation::registerPybindModule(pybind11::module& m)
{
	//guard double registration
	static bool registered = false;
	if (registered) return;
	registered = true;
	
	namespace py = pybind11;
	py::class_<IRayEvaluation>(m, "IRayEvaluation")
		.def_readwrite("volume", &IRayEvaluation::selectedVolume_,
			py::doc("The selected IVolumeINterpolation method"));
}

renderer::IModule_ptr renderer::IRayEvaluation::getSelectedModuleForTag(const std::string& tag)
{
	if (tag == IVolumeInterpolation::Tag())
		return getSelectedVolume();
	return nullptr;
}

std::vector<std::string> renderer::IRayEvaluation::getSupportedTags()
{
	std::vector<std::string> tags;
	if (IModuleContainer_ptr mc = std::dynamic_pointer_cast<IModuleContainer>(selectedVolume_))
	{
		const auto& t = mc->getSupportedTags();
		tags.insert(tags.end(), t.begin(), t.end());
	}
	tags.push_back(selectedVolume_->getTag());
	return tags;
}
