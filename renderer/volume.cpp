#include "volume.h"

#include <fstream>
#include <sstream>
#include <stdexcept>
#include <cuMat/src/Errors.h>
#include <filesystem>
#include <lz4cpp.hpp>

#include "halton_sampler.h"
#include "errors.h"

BEGIN_RENDERER_NAMESPACE

std::unique_ptr<Volume> TheVolume;

const int Volume::BytesPerType[Volume::_TypeCount_] = {
	1, 2, 4 
};

Volume::MipmapLevel::MipmapLevel(Volume* parent, size_t sizeX, size_t sizeY, size_t sizeZ)
	: dataCpu_(new char[sizeX * sizeY * sizeZ * BytesPerType[parent->type()]])
    , dataGpu_(nullptr)
	, sizeX_(sizeX), sizeY_(sizeY), sizeZ_(sizeZ)
	, cpuDataCounter_(0), gpuDataCounter_(0)
	, dataTexLinear_(0)
	, dataTexNearest_(0)
	, parent_(parent)
{
}

Volume::MipmapLevel::~MipmapLevel()
{
	delete[] dataCpu_;
	if (dataTexLinear_ != 0)
		CUMAT_SAFE_CALL(cudaDestroyTextureObject(dataTexLinear_));
	if (dataTexNearest_ != 0)
		CUMAT_SAFE_CALL(cudaDestroyTextureObject(dataTexNearest_));
	if (dataGpu_)
		CUMAT_SAFE_CALL(cudaFreeArray(dataGpu_));
}

void Volume::MipmapLevel::copyCpuToGpu()
{
	if (gpuDataCounter_ == cpuDataCounter_ && dataGpu_)
		return; //nothing changed
	gpuDataCounter_ = cpuDataCounter_;

	//create array
	cudaExtent extent = make_cudaExtent(sizeX_, sizeY_, sizeZ_);
	if (!dataGpu_) {
		cudaChannelFormatDesc channelDesc;
		switch (parent_->type()) {
		case TypeUChar:
			channelDesc = cudaCreateChannelDesc(8, 0, 0, 0, cudaChannelFormatKindUnsigned);
			break;
		case TypeUShort:
			channelDesc = cudaCreateChannelDesc(16, 0, 0, 0, cudaChannelFormatKindUnsigned);
			break;
		case TypeFloat:
			channelDesc = cudaCreateChannelDesc(32, 0, 0, 0, cudaChannelFormatKindFloat);
			break;
		default:
			throw std::runtime_error("unknown enum constant");
		}
		CUMAT_SAFE_CALL(cudaMalloc3DArray(&dataGpu_, &channelDesc, extent));
		std::cout << "Cuda array allocated" << std::endl;
	}
	cudaMemcpy3DParms params = { 0 };
	params.srcPtr = make_cudaPitchedPtr(dataCpu_,
		BytesPerType[parent_->type()] * sizeX_, sizeX_, sizeY_);
	params.dstArray = dataGpu_;
	params.extent = extent;
	params.kind = cudaMemcpyHostToDevice;
	CUMAT_SAFE_CALL(cudaMemcpy3D(&params));

	//create texture object
	cudaResourceDesc resDesc;
	memset(&resDesc, 0, sizeof(cudaResourceDesc));
	resDesc.resType = cudaResourceTypeArray;
	resDesc.res.array.array = dataGpu_;
	cudaTextureDesc texDesc;
	memset(&texDesc, 0, sizeof(cudaTextureDesc));
	texDesc.addressMode[0] = cudaAddressModeClamp;
	texDesc.addressMode[1] = cudaAddressModeClamp;
	texDesc.addressMode[2] = cudaAddressModeClamp;
	texDesc.filterMode = cudaFilterModeLinear;
	texDesc.readMode = parent_->type() == TypeFloat ? cudaReadModeElementType : cudaReadModeNormalizedFloat;
	texDesc.normalizedCoords = 0;
	if (dataTexLinear_ == 0)
		CUMAT_SAFE_CALL(cudaDestroyTextureObject(dataTexLinear_));
	dataTexLinear_ = 0;
	CUMAT_SAFE_CALL(cudaCreateTextureObject(&dataTexLinear_, &resDesc, &texDesc, NULL));

	texDesc.filterMode = cudaFilterModePoint;
	if (dataTexNearest_ == 0)
		CUMAT_SAFE_CALL(cudaDestroyTextureObject(dataTexNearest_));
	dataTexNearest_ = 0;
	CUMAT_SAFE_CALL(cudaCreateTextureObject(&dataTexNearest_, &resDesc, &texDesc, NULL));
}

Volume::Volume()
	: worldSizeX_(1), worldSizeY_(1), worldSizeZ_(1)
	, type_(DataType::TypeUChar)
{
}

Volume::Volume(DataType type, size_t sizeX, size_t sizeY, size_t sizeZ)
	: worldSizeX_(1), worldSizeY_(1), worldSizeZ_(1)
	, type_(type)
{
	levels_.push_back(std::unique_ptr<MipmapLevel>(new MipmapLevel(this, sizeX, sizeY, sizeZ)));
}

Volume::~Volume()
{
}

static const char MAGIC[] = "cvol";

/*
 * FORMAT:
 * magic number "cvol", 4Bytes
 * sizeXYZ, 3*8 Bytes
 * voxelSizeXYZ, 3*8 Bytes,
 * datatype, 4Bytes
 * 8 bytes padding
 * ==> 64 Bytes header
 * Then follows the raw data
 */

void Volume::save(const std::string& filename,
	const VolumeProgressCallback_t& progress,
	const VolumeLoggingCallback_t& logging,
	const VolumeErrorCallback_t& error,
	int compression) const
{
	assert(sizeof(size_t) == 8);
	assert(sizeof(double) == 8);
	std::ofstream s(filename, std::fstream::binary);

	if (compression < 0 || compression > MAX_COMPRESSION)
		throw std::runtime_error("Illegal compression factor");
	
	const MipmapLevel* data = getLevel(0);

	//header
	double voxelSizeX = worldSizeX_ / data->sizeX_;
	double voxelSizeY = worldSizeY_ / data->sizeY_;
	double voxelSizeZ = worldSizeZ_ / data->sizeZ_;
	s.write(MAGIC, 4);
	s.write(reinterpret_cast<const char*>(&data->sizeX_), 8);
	s.write(reinterpret_cast<const char*>(&data->sizeY_), 8);
	s.write(reinterpret_cast<const char*>(&data->sizeZ_), 8);
	s.write(reinterpret_cast<const char*>(&voxelSizeX), 8);
	s.write(reinterpret_cast<const char*>(&voxelSizeY), 8);
	s.write(reinterpret_cast<const char*>(&voxelSizeZ), 8);
	int type = static_cast<int>(type_);
	s.write(reinterpret_cast<const char*>(&type), 4);
	char useCompression = compression > 0 ? 1 : 0;
	s.write(&useCompression, 1);
	char padding[8] = { 0 };
	s.write(padding, 7);

	//body
	progress(0.0f);
	if (useCompression)
	{
		LZ4Compressor c(compression >= LZ4Compressor::MIN_COMPRESSION ? compression : LZ4Compressor::FAST_COMPRESSION);
		size_t dataToWrite = BytesPerType[type_] * data->sizeX_ * data->sizeY_ * data->sizeZ_;
		int chunkSize = LZ4Compressor::MAX_CHUNK_SIZE;
		for (size_t offset=0; offset<dataToWrite; offset+=chunkSize)
		{
			const char* mem = data->dataCpu_ + offset;
			const int len = std::min(static_cast<int>(dataToWrite - offset), chunkSize);
			c.compress(s, mem, len);
			progress(offset / float(dataToWrite));
		}
	} else
	{
		size_t dataToWrite = BytesPerType[type_] * data->sizeX_ * data->sizeY_;
		for (int z = 0; z < data->sizeZ_; ++z)
		{
			s.write(data->dataCpu_ + z * dataToWrite, dataToWrite);
			if (z % 10 == 0)
				progress(z / float(data->sizeZ_));
		}
	}
	progress(1.0f);
}

Volume::Volume(const std::string& filename,
	const VolumeProgressCallback_t& progress,
	const VolumeLoggingCallback_t& logging,
	const VolumeErrorCallback_t& error)
	: Volume()
{
	assert(sizeof(size_t) == 8);
	assert(sizeof(double) == 8);
	std::ifstream s(filename, std::fstream::binary);

	//header
	char magic[4];
	s.read(magic, 4);
	if (memcmp(MAGIC, magic, 4) != 0)
	{
		error("Illegal magic number", -1);
	}
	size_t sizeX, sizeY, sizeZ;
	double voxelSizeX, voxelSizeY, voxelSizeZ;
	char useCompression;
	s.read(reinterpret_cast<char*>(&sizeX), 8);
	s.read(reinterpret_cast<char*>(&sizeY), 8);
	s.read(reinterpret_cast<char*>(&sizeZ), 8);
	s.read(reinterpret_cast<char*>(&voxelSizeX), 8);
	s.read(reinterpret_cast<char*>(&voxelSizeY), 8);
	s.read(reinterpret_cast<char*>(&voxelSizeZ), 8);
	int type;
	s.read(reinterpret_cast<char*>(&type), 4);
	s.read(&useCompression, 1);
	s.ignore(7);
	type_ = static_cast<DataType>(type);

	//create level
	levels_.push_back(std::unique_ptr<MipmapLevel>(new MipmapLevel(this, sizeX, sizeY, sizeZ)));
	MipmapLevel* data = levels_[0].get();
	worldSizeX_ = voxelSizeX * sizeX;
	worldSizeY_ = voxelSizeY * sizeY;
	worldSizeZ_ = voxelSizeZ * sizeZ;

	//body
	progress(0.0f);
	if (useCompression) {
		LZ4Decompressor d;
		size_t dataToRead = BytesPerType[type_] * data->sizeX_ * data->sizeY_ * data->sizeZ_;
		for (size_t offset = 0; offset < dataToRead;)
		{
			char* mem = data->dataCpu_ + offset;
			const int len = std::min(
				static_cast<int>(dataToRead - offset), 
				std::numeric_limits<int>::max());
			int chunkSize = d.decompress(mem, len, s);
			progress(offset / float(dataToRead));
			offset += chunkSize;
		}
	}
	else
	{
		size_t dataToRead = BytesPerType[type_] * data->sizeX_ * data->sizeY_;
		for (int z = 0; z < data->sizeZ_; ++z)
		{
			s.read(data->dataCpu_ + z * dataToRead, dataToRead);
			if (z % 10 == 0)
				progress(z / float(data->sizeZ_));
		}
	}
	progress(1.0f);
}

namespace
{
	//copied and adapted from Pytorch: ATen/native/AdaptiveAveragePooling3d.cpp

	inline int start_index(int a, int b, int c) {
		return (int)std::floor((float)(a * c) / b);
	}

	inline int end_index(int a, int b, int c) {
		return (int)std::ceil((float)((a + 1) * c) / b);
	}
	
	template<typename T>
    void adaptive_avg_pool3d(const Volume::MipmapLevel* in, Volume::MipmapLevel* out)
	{
		const T* dataIn = in->dataCpu<T>();
		T* dataOut = out->dataCpu<T>();
		//fetch sizes
		const int inSizeX  = static_cast<int>(in->sizeX());
		const int inSizeY  = static_cast<int>(in->sizeY());
		const int inSizeZ  = static_cast<int>(in->sizeZ());
		const int outSizeX = static_cast<int>(out->sizeX());
		const int outSizeY = static_cast<int>(out->sizeY());
		const int outSizeZ = static_cast<int>(out->sizeZ());
		//loop over output
#pragma omp parallel for
		for (int oz = 0; oz < outSizeZ; ++oz)
		{
			const int iStartZ = start_index(oz, outSizeZ, inSizeZ);
			const int iEndZ = end_index(oz, outSizeZ, inSizeZ);
			const int kZ = iEndZ - iStartZ;
			for (int oy = 0; oy < outSizeY; ++oy)
			{
				const int iStartY = start_index(oy, outSizeY, inSizeY);
				const int iEndY = end_index(oy, outSizeY, inSizeY);
				const int kY = iEndY - iStartY;
				for (int ox = 0; ox < outSizeX; ++ox)
				{
					const int iStartX = start_index(ox, outSizeX, inSizeX);
					const int iEndX = end_index(ox, outSizeX, inSizeX);
					const int kX = iEndX - iStartX;

					//compute local average
					float sum = 0;
					for (int iz = iStartZ; iz < iEndZ; ++iz)
						for (int iy = iStartY; iy < iEndY; ++iy)
							for (int ix = iStartX; ix < iEndX; ++ix)
								sum += static_cast<float>(dataIn[in->idx(ix, iy, iz)]);
					dataOut[out->idx(ox, oy, oz)] = static_cast<T>(sum / (kX*kY*kZ));
				}
			}
		}
	}

	//Halton-sampling the pixels to use.
	//It uses base 3, 5, 7 for the x,y,z axis
	template<typename T>
	void adaptive_halton_pool3d(const Volume::MipmapLevel* in, Volume::MipmapLevel* out)
	{
		const T* dataIn = in->dataCpu<T>();
		T* dataOut = out->dataCpu<T>();
		//fetch sizes
		const int inSizeX = static_cast<int>(in->sizeX());
		const int inSizeY = static_cast<int>(in->sizeY());
		const int inSizeZ = static_cast<int>(in->sizeZ());
		const int outSizeX = static_cast<int>(out->sizeX());
		const int outSizeY = static_cast<int>(out->sizeY());
		const int outSizeZ = static_cast<int>(out->sizeZ());
		//loop over output
#pragma omp parallel for
		for (int oz = 0; oz < outSizeZ; ++oz)
		{
			const int iStartZ = start_index(oz, outSizeZ, inSizeZ);
			const int iEndZ = end_index(oz, outSizeZ, inSizeZ);
			const int kZ = iEndZ - iStartZ;
			for (int oy = 0; oy < outSizeY; ++oy)
			{
				const int iStartY = start_index(oy, outSizeY, inSizeY);
				const int iEndY = end_index(oy, outSizeY, inSizeY);
				const int kY = iEndY - iStartY;
				for (int ox = 0; ox < outSizeX; ++ox)
				{
					const int iStartX = start_index(ox, outSizeX, inSizeX);
					const int iEndX = end_index(ox, outSizeX, inSizeX);
					const int kX = iEndX - iStartX;

					//get sample index
					const uint64_t sampleIdx = uint64_t(out->idx(ox, oy, oz));
					const int ix = iStartX + int(kX * HaltonSampler::Sample<3, float>(sampleIdx));
					const int iy = iStartY + int(kY * HaltonSampler::Sample<5, float>(sampleIdx));
					const int iz = iStartZ + int(kZ * HaltonSampler::Sample<7, float>(sampleIdx));
					dataOut[out->idx(ox, oy, oz)] = dataIn[in->idx(ix, iy, iz)];
				}
			}
		}
	}
}

void Volume::createMipmapLevel(int level, MipmapFilterMode filter)
{
	switch (filter)
	{
	case MipmapFilterMode::AVERAGE:
		createMipmapLevelAverage(level);
		break;
	case MipmapFilterMode::HALTON:
		createMipmapLevelHalton(level);
		break;
	default:
		throw std::runtime_error("Unknown enum constant");
	}
}

static inline float sqr(float s) { return s * s; }

std::unique_ptr<Volume> Volume::createSyntheticDataset(int resolution, float boxMin, float boxMax,
	const ImplicitFunction_t& f)
{
	auto vol = std::make_unique<Volume>(DataType::TypeFloat, resolution, resolution, resolution);
	auto level = vol->getLevel(0);
	auto data = level->dataCpu<float>();
	float scale = (boxMax - boxMin) / (resolution - 1);
#pragma omp parallel for
	for (int x = 0; x < resolution; ++x)
		for (int y = 0; y < resolution; ++y)
			for (int z = 0; z < resolution; ++z)
			{
				float3 xyz = make_float3(
					boxMin + x * scale, boxMin + y * scale, boxMin + z * scale);
				float v = f(xyz);
				data[level->idx(x, y, z)] = v;
			}
	return vol;
}

std::unique_ptr<Volume> Volume::createFromBuffer(const float* buffer, long long sizes[3], long long strides[3])
{
	//std::cout << "From Buffer: sizes=(" << sizes[0] << "," << sizes[1] << "," << sizes[2]
	//	<< "), strides=(" << strides[0] << "," << strides[1] << "," << strides[2] << ")" << std::endl;
	auto vol = std::make_unique<Volume>(
		DataType::TypeFloat, static_cast<int>(sizes[0]), 
		static_cast<int>(sizes[1]), static_cast<int>(sizes[2]));
	auto level = vol->getLevel(0);
	auto data = level->dataCpu<float>();
#pragma omp parallel for
	for (int x = 0; x < sizes[0]; ++x)
		for (int y = 0; y < sizes[1]; ++y)
			for (int z = 0; z < sizes[2]; ++z)
				data[level->idx(x,y,z)] = buffer[x * strides[0] + y * strides[1] + z * strides[2]];
	return vol;
}

bool Volume::mipmapCheckOrCreate(int level)
{
	CHECK_ERROR(level >= 0, "level has to be non-zero, but is ", level);
	if (level < levels_.size() && levels_[level]) return false; //already available

	//create storage
	if (level >= levels_.size()) levels_.resize(level + 1);
	size_t newSizeX = std::max(size_t(1), levels_[0]->sizeX_ / (level + 1));
	size_t newSizeY = std::max(size_t(1), levels_[0]->sizeY_ / (level + 1));
	size_t newSizeZ = std::max(size_t(1), levels_[0]->sizeZ_ / (level + 1));
	levels_[level] = std::unique_ptr<MipmapLevel>(new MipmapLevel(this, newSizeX, newSizeY, newSizeZ));
	return true;
}

void Volume::createMipmapLevelAverage(int level)
{
	if (!mipmapCheckOrCreate(level)) return; //already available
	auto data = levels_[level].get();

	//perform area filtering
	switch (type_)
	{
	case TypeUChar:
		adaptive_avg_pool3d<unsigned char>(levels_[0].get(), data);
		break;
	case TypeUShort:
		adaptive_avg_pool3d<unsigned short>(levels_[0].get(), data);
		break;
	case TypeFloat:
		adaptive_avg_pool3d<float>(levels_[0].get(), data);
		break;
	default:
		throw std::runtime_error("Unknown enum constant");
	}
}

void Volume::createMipmapLevelHalton(int level)
{
	if (!mipmapCheckOrCreate(level)) return; //already available
	auto data = levels_[level].get();

	//perform area filtering
	switch (type_)
	{
	case TypeUChar:
		adaptive_halton_pool3d<unsigned char>(levels_[0].get(), data);
		break;
	case TypeUShort:
		adaptive_halton_pool3d<unsigned short>(levels_[0].get(), data);
		break;
	case TypeFloat:
		adaptive_halton_pool3d<float>(levels_[0].get(), data);
		break;
	default:
		throw std::runtime_error("Unknown enum constant");
	}
}

void Volume::deleteAllMipmapLevels()
{
	levels_.resize(1); //just keep the first level = original data
}

const Volume::MipmapLevel* Volume::getLevel(int level) const
{
	CHECK_ERROR(level >= 0, "level has to be non-zero, but is ", level);
	if (level >= levels_.size()) return nullptr;
	return levels_[level].get();
}

Volume::MipmapLevel* Volume::getLevel(int level)
{
	CHECK_ERROR(level >= 0, "level has to be non-zero, but is ", level);
	if (level >= levels_.size()) return nullptr;
	return levels_[level].get();
}

void CloseVolume()
{
	TheVolume.reset();
	std::cout << "Volume closed and memory freed" << std::endl;
}

static void printProgress(const std::string& prefix, float progress)
{
	int barWidth = 50;
	std::cout << prefix << " [";
	int pos = static_cast<int>(barWidth * progress);
	for (int i = 0; i < barWidth; ++i) {
		if (i < pos) std::cout << "=";
		else if (i == pos) std::cout << ">";
		else std::cout << " ";
	}
	std::cout << "] " << int(progress * 100.0) << " %\r";
	std::cout.flush();
	if (progress >= 1) std::cout << std::endl;
}

std::shared_ptr<Volume> Volume::loadVolumeFromRaw(
	const std::string& filename, const VolumeProgressCallback_t& progress,
	const VolumeLoggingCallback_t& logging, const VolumeErrorCallback_t& error, 
	const std::optional<int>& ensemble)
{
	auto filename_path = std::filesystem::path(filename);
	//read descriptor file
	if (filename_path.extension() == "dat")
	{
		error("Unrecognized extension, .dat expected : ." + filename_path.extension().string(), -1);
		return nullptr;
	}
	std::ifstream file(filename);
	if (!file.is_open())
	{
		error("Unable to open file " + filename, -1);
		return nullptr;
	}
	std::string line;
	std::string objectFileName = "";
	size_t resolutionX = 0;
	size_t resolutionY = 0;
	size_t resolutionZ = 0;
	double sliceThicknessX = 1;
	double sliceThicknessY = 1;
	double sliceThicknessZ = 1;
	std::string datatype = "";
	const std::string DATATYPE_UCHAR = "UCHAR";
	const std::string DATATYPE_USHORT = "USHORT";
	const std::string DATATYPE_BYTE = "BYTE";
	const std::string DATATYPE_FLOAT = "FLOAT";
	while (std::getline(file, line))
	{
		if (line.empty()) continue;
		std::istringstream iss(line);
		std::string token;
		iss >> token;
		if (!iss) continue; //no token in the current line
		if (token == "ObjectFileName:")
			iss >> objectFileName;
		else if (token == "Resolution:")
			iss >> resolutionX >> resolutionY >> resolutionZ;
		else if (token == "SliceThickness:")
			iss >> sliceThicknessX >> sliceThicknessY >> sliceThicknessZ;
		else if (token == "Format:")
			iss >> datatype;
		if (!iss)
		{
			error("Unable to parse line with token " + token, -2);
			return nullptr;
		}
	}
	file.close();
	if (objectFileName.empty() || resolutionX == 0 || datatype.empty())
	{
		error("Descriptor file does not contain ObjectFileName, Resolution and Format", -3);
		return nullptr;
	}
	if (!(datatype == DATATYPE_UCHAR || datatype == DATATYPE_USHORT || datatype == DATATYPE_BYTE || datatype == DATATYPE_FLOAT))
	{
		error("Unknown format " + datatype, -4);
		return nullptr;
	}

	if (ensemble.has_value())
	{
		char buff[256];
		snprintf(buff, sizeof(buff), objectFileName.c_str(), ensemble.value());
		objectFileName = buff;
	}
	
	logging(std::string("Descriptor file read")
		+ "\nObjectFileName: " + objectFileName
		+ "\nResolution: " + std::to_string(resolutionX) + ", " + std::to_string(resolutionY) + ", " + std::to_string(resolutionZ)
		+ "\nFormat: " + datatype);

	// open volume
	size_t bytesPerEntry = 0;
	if (datatype == DATATYPE_UCHAR) bytesPerEntry = 1;
	if (datatype == DATATYPE_BYTE) bytesPerEntry = 1;
	if (datatype == DATATYPE_USHORT) bytesPerEntry = 2;
	if (datatype == DATATYPE_FLOAT) bytesPerEntry = 4;
	size_t bytesToRead = resolutionX * resolutionY * resolutionZ * bytesPerEntry;
	std::string bfilename = filename_path.replace_filename(objectFileName).generic_string();

	if (bytesToRead > 1024ll * 1024 * 1024 * 16)
	{
		error("Files is too large", -10);
		return nullptr;
	}

	std::cout << "Load " << bytesToRead << " bytes from " << bfilename << std::endl;
	std::ifstream bfile(bfilename, std::ifstream::binary | std::ifstream::ate);
	if (!bfile.is_open())
	{
		error("Unable to open file " + bfilename, -5);
		return nullptr;
	}
	size_t filesize = bfile.tellg();
	int headersize = static_cast<int>(filesize - static_cast<long long>(bytesToRead));
	if (headersize < 0)
	{
		error("File is too small, " + std::to_string(-headersize) + " bytes missing", -6);
		return nullptr;
	}
	std::cout << "Skipping header of length " << headersize << std::endl;
	bfile.seekg(std::ifstream::pos_type(headersize));

	// create output volume and read the data
	bytesToRead = resolutionX * resolutionY * bytesPerEntry;
	std::vector<char> data(bytesToRead);

	std::unique_ptr<Volume> vol;
	if (datatype == DATATYPE_UCHAR || datatype == DATATYPE_BYTE) {
		vol = std::make_unique<Volume>(
			Volume::TypeUChar, resolutionX, resolutionY, resolutionZ);
		Volume::MipmapLevel* level = vol->getLevel(0);
		unsigned char* volumeData = level->dataCpu<unsigned char>();
		const unsigned char* raw = reinterpret_cast<unsigned char*>(data.data());
		for (int z = 0; z < resolutionZ; ++z)
		{
			bfile.read(&data[0], bytesToRead);
			if (!bfile)
			{
				error("Loading data file failed", -7);
				return nullptr;
			}
			if (z % 10 == 0)
				progress(z / float(resolutionZ));
#pragma omp parallel for
			for (int y = 0; y < resolutionY; ++y)
				for (int x = 0; x < resolutionX; ++x)
				{
					unsigned char val = raw[x + resolutionX * y];
					volumeData[level->idx(x, y, z)] = val;
				}
		}
	}
//	else if (datatype == DATATYPE_BYTE) {
//		logging("signed BYTE format not supported, convert to FLOAT");
//		vol = std::make_unique<Volume>(
//			Volume::TypeFloat, resolutionX, resolutionY, resolutionZ);
//		float* volumeData = vol->dataCpu<float>();
//		const float* raw = reinterpret_cast<float*>(data.data());
//		for (int z = 0; z < resolutionZ; ++z)
//		{
//			bfile.read(&data[0], bytesToRead);
//			if (!bfile)
//			{
//				error("Loading data file failed", -7);
//				return nullptr;
//			}
//			if (z % 10 == 0)
//				progress(z / float(resolutionZ));
//#pragma omp parallel for
//			for (int y = 0; y < resolutionY; ++y)
//				for (int x = 0; x < resolutionX; ++x)
//				{
//					float val = raw[x + resolutionX * y] / 255.0f;
//					volumeData[vol->idx(x, y, z)] = val;
//				}
//		}
//	}
	else if (datatype == DATATYPE_USHORT) {
		vol = std::make_unique<Volume>(
			Volume::TypeUShort, resolutionX, resolutionY, resolutionZ);
		Volume::MipmapLevel* level = vol->getLevel(0);
		unsigned short* volumeData = level->dataCpu<unsigned short>();
		const unsigned short* raw = reinterpret_cast<unsigned short*>(data.data());
		for (int z = 0; z < resolutionZ; ++z)
		{
			bfile.read(&data[0], bytesToRead);
			if (!bfile)
			{
				error("Loading data file failed", -7);
				return nullptr;
			}
			if (z % 10 == 0)
				progress(z / float(resolutionZ));
#pragma omp parallel for
			for (int y = 0; y < resolutionY; ++y)
				for (int x = 0; x < resolutionX; ++x)
				{
					unsigned short val = raw[x + resolutionX * y];
					volumeData[level->idx(x, y, z)] = val;
				}
		}
	}
	else if (datatype == DATATYPE_FLOAT) {
		vol = std::make_unique<Volume>(
			Volume::TypeFloat, resolutionX, resolutionY, resolutionZ);
		Volume::MipmapLevel* level = vol->getLevel(0);
		float* volumeData = level->dataCpu<float>();
		const float* raw = reinterpret_cast<float*>(data.data());
		for (int z = 0; z < resolutionZ; ++z)
		{
			bfile.read(&data[0], bytesToRead);
			if (!bfile)
			{
				error("Loading data file failed", -7);
				return nullptr;
			}
			if (z % 10 == 0)
				progress(z / float(resolutionZ));
#pragma omp parallel for
			for (int y = 0; y < resolutionY; ++y)
				for (int x = 0; x < resolutionX; ++x)
				{
					float val = raw[x + resolutionX * y];
					volumeData[level->idx(x, y, z)] = val;
				}
		}
	}
	progress(1.0f);

	// set voxel size, scale so that a box of at most 1x1x1 is occupied
	double maxSize = std::max({
		sliceThicknessX * resolutionX,
		sliceThicknessY * resolutionY,
		sliceThicknessZ * resolutionZ
	});
	vol->setWorldSizeX(sliceThicknessX / maxSize * resolutionX);
	vol->setWorldSizeY(sliceThicknessY / maxSize * resolutionY);
	vol->setWorldSizeZ(sliceThicknessZ / maxSize * resolutionZ);

	//done
	std::stringstream s;
	s << "Reading done, resolution=(" << resolutionX <<
		"," << resolutionY << "," << resolutionZ <<
		"), size=(" << vol->worldSizeX() <<
		"," << vol->worldSizeY() << "," << vol->worldSizeZ() <<
		")";
	logging(s.str());

	return vol;
}


std::shared_ptr<Volume> Volume::loadVolumeFromXYZ(
	const std::string& filename, const VolumeProgressCallback_t& progress,
	const VolumeLoggingCallback_t& logging, const VolumeErrorCallback_t& error)
{
	std::ifstream in(filename, std::ifstream::in | std::ifstream::binary);
	unsigned int sizeX, sizeY, sizeZ;
	double voxelSizeX, voxelSizeY, voxelSizeZ;
	in.read(reinterpret_cast<char*>(&sizeX), sizeof(unsigned int));
	in.read(reinterpret_cast<char*>(&sizeY), sizeof(unsigned int));
	in.read(reinterpret_cast<char*>(&sizeZ), sizeof(unsigned int));
	in.read(reinterpret_cast<char*>(&voxelSizeX), sizeof(double));
	in.read(reinterpret_cast<char*>(&voxelSizeY), sizeof(double));
	in.read(reinterpret_cast<char*>(&voxelSizeZ), sizeof(double));
	unsigned int maxSize = std::max({ sizeX, sizeY, sizeZ });
	voxelSizeX = 1.0 / maxSize;
	voxelSizeY = 1.0 / maxSize;
	voxelSizeZ = 1.0 / maxSize;

	std::unique_ptr<Volume> vol = std::make_unique<Volume>(Volume::TypeFloat, sizeX, sizeY, sizeZ);
	vol->setWorldSizeX(voxelSizeX * sizeX);
	vol->setWorldSizeY(voxelSizeY * sizeY);
	vol->setWorldSizeZ(voxelSizeZ * sizeZ);
	Volume::MipmapLevel* level = vol->getLevel(0);
	float* volumeData = level->dataCpu<float>();

	size_t floatsToRead = sizeZ * sizeY;
	std::vector<float> data(floatsToRead);
	for (unsigned int x = 0; x < sizeX; ++x)
	{
		in.read(reinterpret_cast<char*>(&data[0]), sizeof(float)*floatsToRead);
		if (!in)
		{
			error("Loading data file failed", -7);
			return nullptr;
		}
		if (x % 10 == 0)
			progress(x / float(sizeX));

#pragma omp parallel for
		for (int y = 0; y < int(sizeY); ++y)
			for (int z = 0; z < int(sizeZ); ++z)
				volumeData[level->idx(x, y, z)] = data[z + sizeZ * y];
	}
	progress(1.0f);

	//done
	std::stringstream s;
	s << "Reading done, resolution=(" << sizeX <<
		"," << sizeY << "," << sizeZ <<
		"), size=(" << vol->worldSizeX() <<
		"," << vol->worldSizeY() << "," << vol->worldSizeZ() <<
		")" << std::endl;
	logging(s.str());

	return vol;
}


END_RENDERER_NAMESPACE
