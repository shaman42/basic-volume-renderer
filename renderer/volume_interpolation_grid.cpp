#include "volume_interpolation.h"

#include <sstream>
#include <portable-file-dialogs.h>
#include <magic_enum.hpp>

#include "helper_math.cuh"
#include "IconsFontAwesome5.h"
#include "imgui_internal.h"
#include "pytorch_utils.h"
#include "renderer_commons.cuh"


double3 renderer::IVolumeInterpolation::boxSize() const
{
	return boxMax_ - boxMin_;
}

double3 renderer::IVolumeInterpolation::voxelSize() const
{
	return boxSize() / make_double3(objectResolution() - make_int3(1));
}

const std::string renderer::VolumeInterpolationGrid::VolumeInterpolationNames[] = {
	"Nearest", "Linear", "Cubic"
};

renderer::VolumeInterpolationGrid::VolumeInterpolationGrid()
	: IVolumeInterpolation(true)
   , source_(VolumeSource::EMPTY)
   , interpolation_(VolumeInterpolation::NEAREST_NEIGHBOR)
   , mipmapLevel_(0)
{}

void renderer::VolumeInterpolationGrid::setSource(Volume_ptr v, int mipmap)
{
	source_ = VolumeSource::VOLUME;
	bool changed = volume_ != v;
	volume_ = v;
	mipmapLevel_ = mipmap;
	tensor_ = Parameter<torch::Tensor>();

	double3 worldSize = make_double3(
		v->worldSizeX(), v->worldSizeY(), v->worldSizeZ()
	);
	setObjectResolution(v->getLevel(mipmap)->size());
	setBoxMin(-worldSize / 2.0);
	setBoxMax( worldSize / 2.0);

	if (changed) {
		histogram_ = volume_->extractHistogram();
		minDensity_ = (minDensity_ < histogram_->maxDensity&& minDensity_ > histogram_->minDensity) ? minDensity_ : histogram_->minDensity;
		maxDensity_ = (maxDensity_ < histogram_->maxDensity&& maxDensity_ > histogram_->minDensity) ? maxDensity_ : histogram_->maxDensity;
	}
}

void renderer::VolumeInterpolationGrid::setSource(const torch::Tensor& t)
{
	CHECK_DIM(t, 4);
	TORCH_CHECK((t.dtype() == c10::kFloat || t.dtype() == c10::kDouble),
		"tensor must be of type float or double, but is ", t.dtype());
	
	source_ = VolumeSource::TORCH_TENSOR;
	volume_ = nullptr;
	mipmapLevel_ = 0;
	tensor_.value = t;
	tensor_.grad = torch::Tensor();
	tensor_.forwardIndex = torch::Tensor();

	histogram_ = nullptr;
	minDensity_ = torch::min(t).item().toFloat();
	maxDensity_ = torch::max(t).item().toFloat();

	int3 res = make_int3(t.size(1), t.size(2), t.size(3));
	setObjectResolution(res);
	double voxelSize = 1.0 / std::max({ res.x, res.y, res.z });
	double3 worldSize = make_double3(res) * voxelSize;
	setBoxMin(-worldSize / 2.0);
	setBoxMax(worldSize / 2.0);
}

renderer::Volume_ptr renderer::VolumeInterpolationGrid::volume() const
{
	TORCH_CHECK(source_ == VolumeSource::VOLUME,
		"source()==VOLUME required, but is currently ",
		magic_enum::enum_name(source_));
	return volume_;
}

int renderer::VolumeInterpolationGrid::mipmapLevel() const
{
	TORCH_CHECK(source_ == VolumeSource::VOLUME,
		"source()==VOLUME required, but is currently ",
		magic_enum::enum_name(source_));
	return mipmapLevel_;
}

renderer::Parameter<at::Tensor> renderer::VolumeInterpolationGrid::tensor() const
{
	TORCH_CHECK(source_ == VolumeSource::TORCH_TENSOR,
	            "source()==TORCH_TENSOR required, but is currently ", 
				magic_enum::enum_name(source_));
	return tensor_;
}

std::string renderer::VolumeInterpolationGrid::getName() const
{
	return "Grid";
}

const std::string renderer::VolumeInterpolationGrid::UI_KEY_HISTOGRAM = "volume##histogram";
const std::string renderer::VolumeInterpolationGrid::UI_KEY_MIN_DENSITY = "volume##minDensity";
const std::string renderer::VolumeInterpolationGrid::UI_KEY_MAX_DENSITY = "volume##maxDensity";

bool renderer::VolumeInterpolationGrid::drawUI(UIStorage_t& storage)
{
	bool changed = false;
	
	if (source_ == VolumeSource::TORCH_TENSOR)
	{
		ImGui::TextColored(ImVec4(1, 0, 0, 1),
			"Volume specified by PyTorch Tensor,\ncan't edit in the UI");
		return false;
	}
	auto filename = volumeFullFilename_.filename().string();
	ImGui::InputText("##VolumeInterpolationGridInput", 
		filename.data(), 
		filename.size() + 1, 
		ImGuiInputTextFlags_ReadOnly);
	ImGui::SameLine();
	if (ImGui::Button(ICON_FA_FOLDER_OPEN "##VolumeInterpolationGrid"))
	{
		loadVolumeDialog();
	}

	if (ImGui::SliderInt("Interpolation##VolumeInterpolationGrid",
		reinterpret_cast<int*>(&interpolation_),
		0, static_cast<int>(VolumeInterpolation::_COUNT_)-1,
		VolumeInterpolationNames[static_cast<int>(interpolation_)].c_str()))
	{
		changed = true;
	}
	
	if (source_ != VolumeSource::EMPTY)
	{
		ImGui::Text("Resolution: %d, %d, %d\nDensity: min=%.3f, max=%.3f",
			objectResolution().x, objectResolution().y, objectResolution().z,
			minDensity(), maxDensity());
	}

	//UI Storage
	storage[UI_KEY_HISTOGRAM] = histogram_;
	storage[UI_KEY_MIN_DENSITY] = static_cast<float>(minDensity_);
	storage[UI_KEY_MAX_DENSITY] = static_cast<float>(maxDensity_);
	
	if (backgroundGui_)
		backgroundGui_();

	if (newVolumeLoaded_)
	{
		newVolumeLoaded_ = false;
		changed = true;
	}
	return changed;
}

void renderer::VolumeInterpolationGrid::loadVolumeDialog()
{
	std::cout << "Open file dialog" << std::endl;

	// open file dialog
	std::string volumeDirectory = volumeFullFilename_.parent_path().string();
	auto results = pfd::open_file(
		"Load volume",
		volumeDirectory,
		{ "Volumes", "*.dat *.xyz *.cvol" },
		false
	).result();
	if (results.empty())
		return;
	std::string fileNameStr = results[0];

	std::cout << "Load " << fileNameStr << std::endl;
	auto fileNamePath = std::filesystem::path(fileNameStr);
	ImGui::MarkIniSettingsDirty();
	ImGui::SaveIniSettingsToDisk(GImGui->IO.IniFilename);

	//load the file
	worker_.wait(); //wait for current task
	std::shared_ptr<float> progress = std::make_shared<float>(0);
	auto guiTask = [progress]()
	{
		std::cout << "Progress " << *progress.get() << std::endl;
		if (ImGui::BeginPopupModal("Load Volume", nullptr, ImGuiWindowFlags_AlwaysAutoResize))
		{
			ImGui::ProgressBar(*progress.get(), ImVec2(200, 0));
			ImGui::EndPopup();
		}
	};
	this->backgroundGui_ = guiTask;
	ImGui::OpenPopup("Load Volume");
	auto loaderTask = [fileNameStr, progress, this](BackgroundWorker* worker)
	{
		loadVolume(fileNameStr, progress.get());

		//set it in the GUI and close popup
		this->backgroundGui_ = {};
		ImGui::CloseCurrentPopup();
		newVolumeLoaded_ = true;
	};
	//start background task
	worker_.launch(loaderTask);
}

void renderer::VolumeInterpolationGrid::loadVolume(const std::string& filename, float* progress)
{
	auto fileNamePath = std::filesystem::path(filename);
	//callbacks
	renderer::VolumeProgressCallback_t progressCallback = [progress](float v)
	{
		if (progress) *progress = v * 0.99f;
	};
	renderer::VolumeLoggingCallback_t logging = [](const std::string& msg)
	{
		std::cout << msg << std::endl;
	};
	int errorCode = 1;
	renderer::VolumeErrorCallback_t error = [&errorCode](const std::string& msg, int code)
	{
		errorCode = code;
		std::cerr << msg << std::endl;
		throw std::exception(msg.c_str());
	};
	//load it locally
	try {
		std::shared_ptr<renderer::Volume> volume;
		if (fileNamePath.extension() == ".dat") {
			volume = Volume::loadVolumeFromRaw(filename, progressCallback, logging, error);
		}
		else if (fileNamePath.extension() == ".xyz") {
			volume = Volume::loadVolumeFromXYZ(filename, progressCallback, logging, error);
		}
		else if (fileNamePath.extension() == ".cvol")
			volume = std::make_shared<Volume>(filename, progressCallback, logging, error);
		else {
			std::cerr << "Unrecognized extension: " << fileNamePath.extension() << std::endl;
		}
		if (volume != nullptr) {
			volume->getLevel(0)->copyCpuToGpu();
			setSource(volume, 0);
			volumeFullFilename_ = fileNamePath;
			volumeFullFilename_ = fileNamePath.string();
			std::cout << "Loaded" << std::endl;
		}
	}
	catch (const std::exception& ex)
	{
		std::cerr << "Unable to load volume: " << ex.what() << std::endl;
	}
}

void renderer::VolumeInterpolationGrid::load(const nlohmann::json& json)
{
	source_ = magic_enum::enum_cast<VolumeSource>(json.value("source", "")).
		value_or(VolumeSource::EMPTY);
	interpolation_ = magic_enum::enum_cast<VolumeInterpolation>(json.value("interpolation", "")).
		value_or(VolumeInterpolation::TRILINEAR);

	if (source_ == VolumeSource::VOLUME)
	{
		volumeFullFilename_ = std::filesystem::path();
		volume_ = nullptr;
		histogram_ = nullptr;
		auto filename = std::filesystem::path(json.value("volumePath", ""));
		if (std::filesystem::exists(filename))
		{
			loadVolume(filename.string(), nullptr);
			mipmapLevel_ = json.value("mipmapLevel", 0);
			if (volume_ && mipmapLevel_ != 0)
			{
				volume_->createMipmapLevel(mipmapLevel_, Volume::MipmapFilterMode::AVERAGE);
			}
		}
		if (!volume_)
		{
			std::cerr << "Unable to load volume, is the file valid? " << filename << std::endl;
		}
		else
		{
			volumeFullFilename_ = filename;
			histogram_ = volume_->extractHistogram();
		}
	}
}

void renderer::VolumeInterpolationGrid::save(nlohmann::json& json) const
{
	json["source"] = magic_enum::enum_name(source_);
	json["interpolation"] = magic_enum::enum_name(interpolation_);
	if (source_ == VolumeSource::VOLUME)
	{
		json["volumePath"] = volumeFullFilename_.string();
		json["mipmapLevel"] = mipmapLevel_;
	}
}

void renderer::VolumeInterpolationGrid::registerPybindModule(pybind11::module& m)
{
	namespace py = pybind11;
	py::class_<VolumeInterpolationGrid> c(m, "VolumeInterpolationGrid");
	py::enum_<VolumeSource>(c, "VolumeSource")
		.value("Volume", VolumeSource::VOLUME)
		.value("TorchTensor", VolumeSource::TORCH_TENSOR)
		.value("Empty", VolumeSource::EMPTY)
		.export_values();
	py::enum_<VolumeInterpolation>(c, "VolumeInterpolation")
		.value("NearestNeighbor", VolumeInterpolation::NEAREST_NEIGHBOR)
		.value("Trilinear", VolumeInterpolation::TRILINEAR)
		.value("Tricubic", VolumeInterpolation::TRICUBIC)
		.export_values();
	c.def(py::init<>())
		.def("source", &VolumeInterpolationGrid::source)
		.def("interpolation", &VolumeInterpolationGrid::interpolation)
		.def("setInterpolation", &VolumeInterpolationGrid::setInterpolation)
		.def("minDensity", &VolumeInterpolationGrid::minDensity)
		.def("maxDensity", &VolumeInterpolationGrid::maxDensity)
		.def("volume", &VolumeInterpolationGrid::volume)
		.def("tensor", &VolumeInterpolationGrid::tensor)
		.def("setSource", static_cast<void (VolumeInterpolationGrid::*)(Volume_ptr, int)>(&VolumeInterpolationGrid::setSource))
		.def("setSource", static_cast<void (VolumeInterpolationGrid::*)(const torch::Tensor&)>(&VolumeInterpolationGrid::setSource));
}

std::optional<int> renderer::VolumeInterpolationGrid::getBatches(const GlobalSettings& s) const
{
	if (source_ == VolumeSource::TORCH_TENSOR)
		return tensor_.value.size(0);
	return {};
}

std::string renderer::VolumeInterpolationGrid::getDefines(const GlobalSettings& s) const
{
	std::stringstream ss;
	if (s.volumeShouldProvideNormals)
		ss << "#define VOLUME_INTERPOLATION_GRID__REQUIRES_NORMAL\n";
	if (source_ == VolumeSource::TORCH_TENSOR)
		ss << "#define VOLUME_INTERPOLATION_GRID__USE_TENSOR\n";
	switch (interpolation_)
	{
	case VolumeInterpolation::NEAREST_NEIGHBOR:
		ss << "#define VOLUME_INTERPOLATION_GRID__INTERPOLATION 0\n";
		break;
	case VolumeInterpolation::TRILINEAR:
		ss << "#define VOLUME_INTERPOLATION_GRID__INTERPOLATION 1\n";
		break;
	case VolumeInterpolation::TRICUBIC:
		ss << "#define VOLUME_INTERPOLATION_GRID__INTERPOLATION 2\n";
		break;
	}
	if (s.interpolationInObjectSpace)
		ss << "#define VOLUME_INTERPOLATION_GRID__OBJECT_SPACE\n";
	return ss.str();
}

std::vector<std::string> renderer::VolumeInterpolationGrid::getIncludeFileNames(const GlobalSettings& s) const
{
	return {
		"renderer_volume_grid.cuh"
	};
}

std::string renderer::VolumeInterpolationGrid::getConstantDeclarationName(const GlobalSettings& s) const
{
	return "volumeInterpolationGridParameters";
}

std::string renderer::VolumeInterpolationGrid::getPerThreadType(const GlobalSettings& s) const
{
	return "::kernel::VolumeInterpolationGrid";
}

void renderer::VolumeInterpolationGrid::fillConstantMemory(
	const GlobalSettings& s, CUdeviceptr ptr, CUstream stream)
{
	switch (source_)
	{
	case VolumeSource::EMPTY:
		throw std::runtime_error("No volume specified, can't render!");
	case VolumeSource::TORCH_TENSOR:
	{
		throw std::runtime_error("torch::Tensor as volume input is not implemented yet");
	}break;
	case VolumeSource::VOLUME:
	{
		RENDERER_DISPATCH_FLOATING_TYPES(s.scalarType, "VolumeInterpolationGrid", [&]()
		{
			using real3 = typename ::kernel::scalar_traits<scalar_t>::real3;
			struct Parameters
			{
				cudaTextureObject_t tex;
				int3 resolutionMinusOne; //resolution-1
				real3 boxMin;
				real3 boxSize;
				real3 normalStep;
				real3 normalScale;
			} p;
			if (interpolation_ == VolumeInterpolation::NEAREST_NEIGHBOR)
				p.tex = volume_->getLevel(mipmapLevel_)->dataTexGpuNearest();
			else
				p.tex = volume_->getLevel(mipmapLevel_)->dataTexGpuLinear();
			p.resolutionMinusOne = objectResolution() - make_int3(1);
			p.boxMin = kernel::cast3<scalar_t>(boxMin());
			p.boxSize = kernel::cast3<scalar_t>(boxSize());
			double3 voxelSize = boxSize() / make_double3(objectResolution());
			p.normalScale = kernel::cast3<scalar_t>(1.0 / voxelSize);
			double3 normalStep = make_double3(1);//make_double3(1.0) / make_double3(objectResolution());
			p.normalStep = kernel::cast3<scalar_t>(normalStep);
			CU_SAFE_CALL(cuMemcpyHtoDAsync(ptr, &p, sizeof(Parameters), stream));
		});
	}break;
	}
}


