#pragma once

#include "renderer_tensor.cuh"
#include "renderer_utils.cuh"

#include <forward_vector.h>
#include "renderer_cudad_bridge.cuh"
#include "renderer_adjoint.cuh"

#ifdef __CUDACC__
#include "cooperative_groups.cuh"
#endif
//#include <cooperative_groups/reduce.h>

namespace kernel
{
	struct TransferFunctionTextureParameters
	{
#ifdef TRANSFER_FUNCTION_TEXTURE__USE_TENSOR
		Tensor3Read<real_t> tensor;
#else
		cudaTextureObject_t tex;
#endif
	};
}

__constant__ kernel::TransferFunctionTextureParameters transferFunctionTextureParameters;

namespace kernel
{
	struct TransferFunctionTexture
	{

		__host__ __device__ __inline__ real4 eval(
			real_t density, real3 normal, real_t previousDensity, real_t stepsize, int batch) const
		{
			density = clamp(density, real_t(0), real_t(1));
#ifdef TRANSFER_FUNCTION_TEXTURE__USE_TENSOR
			const auto& tf = transferFunctionTextureParameters.tensor;
			const int R = tf.size(1);
			const real_t d = density * R - real_t(0.5);
			const int di = int(floorf(d));
			const real_t df = d - di;
			const real4 val0 = fetchReal4(tf, batch, clamp(di, 0, R - 1));
			const real4 val1 = fetchReal4(tf, batch, clamp(di + 1, 0, R - 1));
			real4 rgba = lerp(val0, val1, df);
#else
			real4 rgba = make_real4(tex1D<float4>(
				transferFunctionTextureParameters.tex, static_cast<float>(density)));
#endif
			rgba.w *= stepsize;
			return rgba;
		}
		
	};
}
