#include "ray_evaluation_stepping.h"

#include "helper_math.cuh"
#include "module_registry.h"
#include "renderer_tensor.cuh"
#include "pytorch_utils.h"

bool renderer::IRayEvaluationStepping::stepsizeCanBeInObjectSpace()
{
	const auto v = getSelectedVolume();
	return v && v->supportsObjectSpaceIndexing();
}

double renderer::IRayEvaluationStepping::getStepsizeWorld()
{
	if (stepsizeIsObjectSpace_ && stepsizeCanBeInObjectSpace())
	{
		return stepsize_ * max_coeff(getSelectedVolume()->voxelSize());
	}
	return stepsize_;
}

bool renderer::IRayEvaluationStepping::drawStepsizeUI(UIStorage_t& storage)
{
	bool changed = false;
	float stepMin = 0.01f, stepMax = 1.0f;
	if (ImGui::SliderDouble("Stepsize##IRayEvaluationStepping", &stepsize_, stepMin, stepMax, "%.5f", 2))
		changed = true;

	if (stepsizeCanBeInObjectSpace())
	{
		if (ImGui::Checkbox("Object space##IRayEvaluationStepping", &stepsizeIsObjectSpace_))
			changed = true;
	}

	return changed;
}

void renderer::IRayEvaluationStepping::load(const nlohmann::json& json)
{
	IRayEvaluation::load(json);
	stepsize_ = json.value("stepsize", 0.5);
	stepsizeIsObjectSpace_ = json.value("stepsizeIsObjectSpace", true);
}

void renderer::IRayEvaluationStepping::save(nlohmann::json& json) const
{
	IRayEvaluation::save(json);
	json["stepsize"] = stepsize_;
	json["stepsizeIsObjectSpace"] = stepsizeIsObjectSpace_;
}

void renderer::IRayEvaluationStepping::registerPybindModule(pybind11::module& m)
{
	IRayEvaluation::registerPybindModule(m);

	//guard double registration
	static bool registered = false;
	if (registered) return;
	registered = true;
	
	namespace py = pybind11;
	py::class_<IRayEvaluationStepping, IRayEvaluation>(m, "IRayEvaluationStepping")
		.def_readwrite("stepsize", &IRayEvaluationStepping::stepsize_)
		.def_readwrite("stepsizeIsObjectSpace", &IRayEvaluationStepping::stepsizeIsObjectSpace_);
}


renderer::RayEvaluationSteppingIso::RayEvaluationSteppingIso()
	: isovalue_(0.5)
{
}

std::string renderer::RayEvaluationSteppingIso::getName() const
{
	return "Iso";
}

void renderer::RayEvaluationSteppingIso::prepareRendering(GlobalSettings& s) const
{
	s.volumeShouldProvideNormals = true;
}

std::optional<int> renderer::RayEvaluationSteppingIso::getBatches(const GlobalSettings& s) const
{
	bool batched = std::holds_alternative<torch::Tensor>(isovalue_.value);
	if (batched)
	{
		torch::Tensor t = std::get<torch::Tensor>(isovalue_.value);
		CHECK_CUDA(t, true);
		CHECK_DIM(t, 1);
		return t.size(0);
	}
	return {};
}

std::string renderer::RayEvaluationSteppingIso::getDefines(const GlobalSettings& s) const
{
	if (!getSelectedVolume()) return "";

	std::stringstream ss;
	ss << "#define RAY_EVALUATION_STEPPING__VOLUME_INTERPOLATION_T "
		<< getSelectedVolume()->getPerThreadType(s)
		<< "\n";
	bool batched = std::holds_alternative<torch::Tensor>(isovalue_.value);
	if (batched)
		ss << "#define RAY_EVALUATION_STEPPING__ISOVALUE_BATCHED\n";
	return ss.str();
}

std::vector<std::string> renderer::RayEvaluationSteppingIso::getIncludeFileNames(const GlobalSettings& s) const
{
	return { "renderer_ray_evaluation_stepping_iso.cuh" };
}

std::string renderer::RayEvaluationSteppingIso::getConstantDeclarationName(const GlobalSettings& s) const
{
	return "rayEvaluationSteppingIsoParameters";
}

std::string renderer::RayEvaluationSteppingIso::getPerThreadType(const GlobalSettings& s) const
{
	return "::kernel::RayEvaluationSteppingIso";
}

void renderer::RayEvaluationSteppingIso::fillConstantMemory(const GlobalSettings& s, CUdeviceptr ptr, CUstream stream)
{
	if (!getSelectedVolume()) throw std::runtime_error("No volume specified!");

	bool batched = std::holds_alternative<torch::Tensor>(isovalue_.value);
	if (batched)
	{
		torch::Tensor t = std::get<torch::Tensor>(isovalue_.value);
		CHECK_CUDA(t, true);
		CHECK_DIM(t, 1);
		CHECK_DTYPE(t, s.scalarType);
		RENDERER_DISPATCH_FLOATING_TYPES(s.scalarType, "RayEvaluationSteppingIso", [&]()
			{
				struct Parameters
				{
					scalar_t stepsize;
					::kernel::Tensor1Read<scalar_t> isovalue;
				} p;
				p.stepsize = static_cast<scalar_t>(getStepsizeWorld());
				p.isovalue = accessor<::kernel::Tensor1Read<scalar_t>>(t);
				CU_SAFE_CALL(cuMemcpyHtoDAsync(ptr, &p, sizeof(Parameters), stream));
			});
	}
	else
	{
		double value = std::get<double>(isovalue_.value);
		RENDERER_DISPATCH_FLOATING_TYPES(s.scalarType, "RayEvaluationSteppingIso", [&]()
			{
				struct Parameters
				{
					scalar_t stepsize;
					scalar_t isovalue;
				} p;
				p.stepsize = static_cast<scalar_t>(getStepsizeWorld());
				p.isovalue = static_cast<scalar_t>(value);
				CU_SAFE_CALL(cuMemcpyHtoDAsync(ptr, &p, sizeof(Parameters), stream));
			});
	}
}

bool renderer::RayEvaluationSteppingIso::drawUI(UIStorage_t& storage)
{
	bool changed = IRayEvaluation::drawUI(storage);

	if (ImGui::CollapsingHeader("Renderer##IRayEvaluation", ImGuiTreeNodeFlags_DefaultOpen))
	{
		if (drawStepsizeUI(storage))
			changed = true;

		//get min, max density from storage
		float minDensity = 0, maxDensity = 1;
		if (const auto it = storage.find(VolumeInterpolationGrid::UI_KEY_MIN_DENSITY);
			it != storage.end())
		{
			minDensity = std::any_cast<float>(it->second);
		}
		if (const auto it = storage.find(VolumeInterpolationGrid::UI_KEY_MAX_DENSITY);
			it != storage.end())
		{
			maxDensity = std::any_cast<float>(it->second);
		}

		if (ImGui::SliderDouble("Isovalue##RayEvaluationSteppingIso",
			enforceAndGetScalar<double>(isovalue_), minDensity, maxDensity))
			changed = true;
	}

	return changed;
}

void renderer::RayEvaluationSteppingIso::load(const nlohmann::json& json)
{
	IRayEvaluationStepping::load(json);
	*enforceAndGetScalar<double>(isovalue_) = json.value("isovalue", 0.5);
}

void renderer::RayEvaluationSteppingIso::save(nlohmann::json& json) const
{
	IRayEvaluationStepping::save(json);
	json["isovalue"] = *getScalarOrThrow<double>(isovalue_);
}

void renderer::RayEvaluationSteppingIso::registerPybindModule(pybind11::module& m)
{
	IRayEvaluationStepping::registerPybindModule(m);

	//guard double registration
	static bool registered = false;
	if (registered) return;
	registered = true;
	
	namespace py = pybind11;
	py::class_<RayEvaluationSteppingIso, IRayEvaluationStepping>(m, "RayEvaluationSteppingIso")
		.def(py::init<>())
		.def_readonly("isovalue", &RayEvaluationSteppingIso::isovalue_,
			py::doc("double with the isovalue (possible batched as (B,) tensor)"));
}


renderer::RayEvaluationSteppingDvr::RayEvaluationSteppingDvr()
	: alphaEarlyOut_(1.0 - 1e-5)
	, minDensity_(0)
	, maxDensity_(1)
{
	const auto bx = ModuleRegistry::Instance().getModulesForTag(
		std::string(Blending::TAG));
	TORCH_CHECK(bx.size() == 1, "There should only be a single blending instance registered");
	blending_ = std::dynamic_pointer_cast<Blending>(bx[0]);
}

std::string renderer::RayEvaluationSteppingDvr::getName() const
{
	return "DVR";
}

void renderer::RayEvaluationSteppingDvr::prepareRendering(GlobalSettings& s) const
{
	IRayEvaluation::prepareRendering(s);
	//nothing to do otherwise (for now)
}

std::string renderer::RayEvaluationSteppingDvr::getDefines(const GlobalSettings& s) const
{
	if (!getSelectedVolume()) return "";

	std::stringstream ss;
	ss << "#define RAY_EVALUATION_STEPPING__VOLUME_INTERPOLATION_T "
		<< getSelectedVolume()->getPerThreadType(s)
		<< "\n";
	ss << "#define RAY_EVALUATION_STEPPING__TRANSFER_FUNCTION_T "
		<< getSelectedTF()->getPerThreadType(s)
		<< "\n";
	ss << "#define RAY_EVALUATION_STEPPING__BLENDING_T "
		<< getSelectedBlending()->getPerThreadType(s)
		<< "\n";
	ss << "#define RAY_EVALUATION_STEPPING__BRDF_T "
		<< getSelectedBRDF()->getPerThreadType(s)
		<< "\n";
	return ss.str();
}

std::vector<std::string> renderer::RayEvaluationSteppingDvr::getIncludeFileNames(const GlobalSettings& s) const
{
	return { "renderer_ray_evaluation_stepping_dvr.cuh" };
}

std::string renderer::RayEvaluationSteppingDvr::getConstantDeclarationName(const GlobalSettings& s) const
{
	return "rayEvaluationSteppingDvrParameters";
}

std::string renderer::RayEvaluationSteppingDvr::getPerThreadType(const GlobalSettings& s) const
{
	return "::kernel::RayEvaluationSteppingDvr";
}

void renderer::RayEvaluationSteppingDvr::fillConstantMemory(const GlobalSettings& s, CUdeviceptr ptr, CUstream stream)
{
	RENDERER_DISPATCH_FLOATING_TYPES(s.scalarType, "RayEvaluationSteppingDvr", [&]()
		{
			struct Parameters
			{
				scalar_t stepsize;
				scalar_t alphaEarlyOut;
				scalar_t densityMin;
				scalar_t densityMax;
			} p;
			p.stepsize = static_cast<scalar_t>(getStepsizeWorld());
			p.alphaEarlyOut = static_cast<scalar_t>(alphaEarlyOut_);
			p.densityMin = static_cast<scalar_t>(minDensity_);
			p.densityMax = static_cast<scalar_t>(maxDensity_);
			CU_SAFE_CALL(cuMemcpyHtoDAsync(ptr, &p, sizeof(Parameters), stream));
		});
}

bool renderer::RayEvaluationSteppingDvr::drawUI(UIStorage_t& storage)
{
	bool changed = IRayEvaluation::drawUI(storage);

	//TF
	const auto& tfs =
		ModuleRegistry::Instance().getModulesForTag(ITransferFunction::Tag());
	if (!tf_)
		tf_ = std::dynamic_pointer_cast<ITransferFunction>(tfs[0]);
	if (ImGui::CollapsingHeader("Transfer Function##IRayEvaluation", ImGuiTreeNodeFlags_DefaultOpen))
	{
		for (int i = 0; i < tfs.size(); ++i) {
			const auto& name = tfs[i]->getName();
			if (ImGui::RadioButton(name.c_str(), tfs[i] == tf_)) {
				tf_ = std::dynamic_pointer_cast<ITransferFunction>(tfs[i]);
				changed = true;
			}
			if (i < tfs.size() - 1) ImGui::SameLine();
		}
		if (tf_->drawUI(storage))
			changed = true;
	}

	//rendering parameters
	if (ImGui::CollapsingHeader("Renderer##IRayEvaluation", ImGuiTreeNodeFlags_DefaultOpen))
	{
		if (drawStepsizeUI(storage))
			changed = true;

		//get min, max density from storage
		float minDensity = 0, maxDensity = 1;
		if (const auto it = storage.find(VolumeInterpolationGrid::UI_KEY_MIN_DENSITY);
			it != storage.end())
		{
			minDensity = std::any_cast<float>(it->second);
		}
		if (const auto it = storage.find(VolumeInterpolationGrid::UI_KEY_MAX_DENSITY);
			it != storage.end())
		{
			maxDensity = std::any_cast<float>(it->second);
		}

		minDensity_ = fmax(minDensity_, minDensity);
		maxDensity_ = fmin(maxDensity_, maxDensity);
		if (ImGui::SliderDouble("Min Density", &minDensity_, minDensity, maxDensity))
			changed = true;
		if (ImGui::SliderDouble("Max Density", &maxDensity_, minDensity, maxDensity))
			changed = true;
		storage[UI_KEY_SELECTED_MIN_DENSITY] = static_cast<double>(minDensity_);
		storage[UI_KEY_SELECTED_MAX_DENSITY] = static_cast<double>(maxDensity_);

		if (blending_->drawUI(storage))
			changed = true;
	}

	//BRDF
	const auto& brdfs =
		ModuleRegistry::Instance().getModulesForTag(IBRDF::Tag());
	if (!brdf_)
		brdf_ = std::dynamic_pointer_cast<IBRDF>(brdfs[0]);
	if (ImGui::CollapsingHeader("BRDF##IRayEvaluation", ImGuiTreeNodeFlags_DefaultOpen))
	{
		for (int i = 0; i < brdfs.size(); ++i) {
			const auto& name = brdfs[i]->getName();
			if (ImGui::RadioButton(name.c_str(), brdfs[i] == brdf_)) {
				brdf_ = std::dynamic_pointer_cast<IBRDF>(brdfs[i]);
				changed = true;
			}
			if (i < brdfs.size() - 1) ImGui::SameLine();
		}
		if (brdf_->drawUI(storage))
			changed = true;
	}

	return changed;
}

renderer::IModule_ptr renderer::RayEvaluationSteppingDvr::getSelectedModuleForTag(const std::string& tag)
{
	if (tag == Blending::TAG)
		return blending_;
	else if (tag == ITransferFunction::TAG)
		return tf_;
	else if (tag == IBRDF::TAG)
		return brdf_;
	else
		return IRayEvaluation::getSelectedModuleForTag(tag);
}

std::vector<std::string> renderer::RayEvaluationSteppingDvr::getSupportedTags()
{
	std::vector<std::string> tags = IRayEvaluation::getSupportedTags();
	tags.push_back(blending_->getTag());
	if (IModuleContainer_ptr mc = std::dynamic_pointer_cast<IModuleContainer>(blending_))
	{
		const auto& t = mc->getSupportedTags();
		tags.insert(tags.end(), t.begin(), t.end());
	}
	tags.push_back(tf_->getTag());
	if (IModuleContainer_ptr mc = std::dynamic_pointer_cast<IModuleContainer>(tf_))
	{
		const auto& t = mc->getSupportedTags();
		tags.insert(tags.end(), t.begin(), t.end());
	}
	tags.push_back(brdf_->getTag());
	if (IModuleContainer_ptr mc = std::dynamic_pointer_cast<IModuleContainer>(brdf_))
	{
		const auto& t = mc->getSupportedTags();
		tags.insert(tags.end(), t.begin(), t.end());
	}
	return tags;
}

void renderer::RayEvaluationSteppingDvr::load(const nlohmann::json& json)
{
	std::string tfName = json.value("selectedTF", "");
	const auto& tfs =
		ModuleRegistry::Instance().getModulesForTag(ITransferFunction::Tag());
	for (const auto v : tfs)
	{
		if (v->getName() == tfName) {
			tf_ = std::dynamic_pointer_cast<ITransferFunction>(v);
			break;
		}
	}

	std::string brdfName = json.value("selectedBRDF", "");
	const auto& brdfs =
		ModuleRegistry::Instance().getModulesForTag(IBRDF::Tag());
	for (const auto v : brdfs)
	{
		if (v->getName() == brdfName) {
			brdf_ = std::dynamic_pointer_cast<IBRDF>(v);
			break;
		}
	}

	minDensity_ = json.value("minDensity", 0.0);
	maxDensity_ = json.value("maxDensity", 1.0);
}

void renderer::RayEvaluationSteppingDvr::save(nlohmann::json& json) const
{
	json["selectedTF"] = tf_->getName();
	json["selectedBRDF"] = brdf_->getName();
	json["minDensity"] = minDensity_;
	json["maxDensity"] = maxDensity_;
}

void renderer::RayEvaluationSteppingDvr::registerPybindModule(pybind11::module& m)
{
	IRayEvaluationStepping::registerPybindModule(m);

	//guard double registration
	static bool registered = false;
	if (registered) return;
	registered = true;
	
	namespace py = pybind11;
	py::class_<RayEvaluationSteppingDvr, IRayEvaluationStepping>(m, "RayEvaluationSteppingDvr")
		.def(py::init<>())
		.def_readwrite("min_density", &RayEvaluationSteppingDvr::minDensity_)
		.def_readwrite("max_density", &RayEvaluationSteppingDvr::maxDensity_)
		.def_readonly("blending", &RayEvaluationSteppingDvr::blending_)
		.def_readwrite("tf", &RayEvaluationSteppingDvr::tf_)
		.def_readwrite("brdf", &RayEvaluationSteppingDvr::brdf_);
}
