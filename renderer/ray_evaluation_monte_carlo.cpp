#include "ray_evaluation_monte_carlo.h"

#include "helper_math.cuh"
#include "module_registry.h"
#include "renderer_tensor.cuh"
#include "pytorch_utils.h"

renderer::RayEvaluationMonteCarlo::RayEvaluationMonteCarlo()
	: minDensity_(0)
	, maxDensity_(1)
	, scatteringFactor_(0.2)
{
}

std::string renderer::RayEvaluationMonteCarlo::getName() const
{
	return "MonteCarlo";
}

void renderer::RayEvaluationMonteCarlo::prepareRendering(GlobalSettings& s) const
{
	IRayEvaluation::prepareRendering(s);
	//nothing to do otherwise (for now)
}

std::string renderer::RayEvaluationMonteCarlo::getDefines(const GlobalSettings& s) const
{
	if (!getSelectedVolume()) return "";

	std::stringstream ss;
	ss << "#define RAY_EVALUATION_MONTE_CARLO__VOLUME_INTERPOLATION_T "
		<< getSelectedVolume()->getPerThreadType(s)
		<< "\n";
	ss << "#define RAY_EVALUATION_MONTE_CARLO__TRANSFER_FUNCTION_T "
		<< getSelectedTF()->getPerThreadType(s)
		<< "\n";
	ss << "#define RAY_EVALUATION_MONTE_CARLO__BRDF_T "
		<< getSelectedBRDF()->getPerThreadType(s)
		<< "\n";
	ss << "#define RAY_EVALUATION_MONTE_CARLO__SAMPLING_T void\n";
	return ss.str();
}

std::vector<std::string> renderer::RayEvaluationMonteCarlo::getIncludeFileNames(const GlobalSettings& s) const
{
	return { "renderer_ray_evaluation_monte_carlo.cuh" };
}

std::string renderer::RayEvaluationMonteCarlo::getConstantDeclarationName(const GlobalSettings& s) const
{
	return "rayEvaluationMonteCarloParameters";
}

std::string renderer::RayEvaluationMonteCarlo::getPerThreadType(const GlobalSettings& s) const
{
	return "::kernel::RayEvaluationMonteCarlo";
}

void renderer::RayEvaluationMonteCarlo::fillConstantMemory(const GlobalSettings& s, CUdeviceptr ptr, CUstream stream)
{
	RENDERER_DISPATCH_FLOATING_TYPES(s.scalarType, "RayEvaluationMonteCarlo", [&]()
		{
			struct Parameters
			{
				scalar_t maxAbsorption;
				scalar_t densityMin;
				scalar_t densityMax;
				scalar_t scatteringFactor;
			} p;
			p.maxAbsorption = static_cast<scalar_t>(getSelectedTF()->getMaxAbsorption());
			p.densityMin = static_cast<scalar_t>(minDensity_);
			p.densityMax = static_cast<scalar_t>(maxDensity_);
			p.scatteringFactor = static_cast<scalar_t>(scatteringFactor_);
			CU_SAFE_CALL(cuMemcpyHtoDAsync(ptr, &p, sizeof(Parameters), stream));
		});
}

bool renderer::RayEvaluationMonteCarlo::drawUI(UIStorage_t& storage)
{
	bool changed = IRayEvaluation::drawUI(storage);

	//TF
	const auto& tfs =
		ModuleRegistry::Instance().getModulesForTag(ITransferFunction::Tag());
	if (!tf_)
		tf_ = std::dynamic_pointer_cast<ITransferFunction>(tfs[0]);
	if (ImGui::CollapsingHeader("Transfer Function##IRayEvaluation", ImGuiTreeNodeFlags_DefaultOpen))
	{
		for (int i = 0; i < tfs.size(); ++i) {
			const auto& name = tfs[i]->getName();
			if (ImGui::RadioButton(name.c_str(), tfs[i] == tf_)) {
				tf_ = std::dynamic_pointer_cast<ITransferFunction>(tfs[i]);
				changed = true;
			}
			if (i < tfs.size() - 1) ImGui::SameLine();
		}
		if (tf_->drawUI(storage))
			changed = true;
	}

	//rendering parameters
	if (ImGui::CollapsingHeader("Renderer##IRayEvaluation", ImGuiTreeNodeFlags_DefaultOpen))
	{
		//get min, max density from storage
		float minDensity = 0, maxDensity = 1;
		if (const auto it = storage.find(VolumeInterpolationGrid::UI_KEY_MIN_DENSITY);
			it != storage.end())
		{
			minDensity = std::any_cast<float>(it->second);
		}
		if (const auto it = storage.find(VolumeInterpolationGrid::UI_KEY_MAX_DENSITY);
			it != storage.end())
		{
			maxDensity = std::any_cast<float>(it->second);
		}

		minDensity_ = fmax(minDensity_, minDensity);
		maxDensity_ = fmin(maxDensity_, maxDensity);
		if (ImGui::SliderDouble("Min Density", &minDensity_, minDensity, maxDensity))
			changed = true;
		if (ImGui::SliderDouble("Max Density", &maxDensity_, minDensity, maxDensity))
			changed = true;
		storage[UI_KEY_SELECTED_MIN_DENSITY] = static_cast<double>(minDensity_);
		storage[UI_KEY_SELECTED_MAX_DENSITY] = static_cast<double>(maxDensity_);

		if (ImGui::SliderDouble("Scattering##RayEvaluationMonteCarlo",
			&scatteringFactor_, 0.0, 1.0))
			changed = true;
	}

	//BRDF
	const auto& brdfs =
		ModuleRegistry::Instance().getModulesForTag(IBRDF::Tag());
	if (!brdf_)
		brdf_ = std::dynamic_pointer_cast<IBRDF>(brdfs[0]);
	if (ImGui::CollapsingHeader("BRDF##IRayEvaluation", ImGuiTreeNodeFlags_DefaultOpen))
	{
		for (int i = 0; i < brdfs.size(); ++i) {
			const auto& name = brdfs[i]->getName();
			if (ImGui::RadioButton(name.c_str(), brdfs[i] == brdf_)) {
				brdf_ = std::dynamic_pointer_cast<IBRDF>(brdfs[i]);
				changed = true;
			}
			if (i < brdfs.size() - 1) ImGui::SameLine();
		}
		if (brdf_->drawUI(storage))
			changed = true;
	}

	return changed;
}

renderer::IModule_ptr renderer::RayEvaluationMonteCarlo::getSelectedModuleForTag(const std::string& tag)
{
	if (tag == ITransferFunction::TAG)
		return tf_;
	else if (tag == IBRDF::TAG)
		return brdf_;
	else
		return IRayEvaluation::getSelectedModuleForTag(tag);
}

std::vector<std::string> renderer::RayEvaluationMonteCarlo::getSupportedTags()
{
	std::vector<std::string> tags = IRayEvaluation::getSupportedTags();
	tags.push_back(tf_->getTag());
	if (IModuleContainer_ptr mc = std::dynamic_pointer_cast<IModuleContainer>(tf_))
	{
		const auto& t = mc->getSupportedTags();
		tags.insert(tags.end(), t.begin(), t.end());
	}
	tags.push_back(brdf_->getTag());
	if (IModuleContainer_ptr mc = std::dynamic_pointer_cast<IModuleContainer>(brdf_))
	{
		const auto& t = mc->getSupportedTags();
		tags.insert(tags.end(), t.begin(), t.end());
	}
	return tags;
}

void renderer::RayEvaluationMonteCarlo::load(const nlohmann::json& json)
{
	std::string tfName = json.value("selectedTF", "");
	const auto& tfs =
		ModuleRegistry::Instance().getModulesForTag(ITransferFunction::Tag());
	for (const auto v : tfs)
	{
		if (v->getName() == tfName) {
			tf_ = std::dynamic_pointer_cast<ITransferFunction>(v);
			break;
		}
	}

	std::string brdfName = json.value("selectedBRDF", "");
	const auto& brdfs =
		ModuleRegistry::Instance().getModulesForTag(IBRDF::Tag());
	for (const auto v : brdfs)
	{
		if (v->getName() == brdfName) {
			brdf_ = std::dynamic_pointer_cast<IBRDF>(v);
			break;
		}
	}

	minDensity_ = json.value("minDensity", 0.0);
	maxDensity_ = json.value("maxDensity", 1.0);
	scatteringFactor_ = json.value("scatteringFactor", 0.5);
}

void renderer::RayEvaluationMonteCarlo::save(nlohmann::json& json) const
{
	json["selectedTF"] = tf_ ? tf_->getName() : "";
	json["selectedBRDF"] = brdf_ ? brdf_->getName() : "";
	json["minDensity"] = minDensity_;
	json["maxDensity"] = maxDensity_;
	json["scatteringFactor"] = scatteringFactor_;
}

void renderer::RayEvaluationMonteCarlo::registerPybindModule(pybind11::module& m)
{
	IRayEvaluation::registerPybindModule(m);

	//guard double registration
	static bool registered = false;
	if (registered) return;
	registered = true;
	
	namespace py = pybind11;
	py::class_<RayEvaluationMonteCarlo, IRayEvaluation>(m, "RayEvaluationMonteCarlo")
		.def(py::init<>())
		.def_readwrite("min_density", &RayEvaluationMonteCarlo::minDensity_)
		.def_readwrite("max_density", &RayEvaluationMonteCarlo::maxDensity_)
		.def_readwrite("scattering_factor", &RayEvaluationMonteCarlo::scatteringFactor_)
		.def_readwrite("tf", &RayEvaluationMonteCarlo::tf_)
		.def_readwrite("brdf", &RayEvaluationMonteCarlo::brdf_);
}
